#include "GLAD/glad.h"
#include "GLFW/glfw3.h"
#include "AL/al.h"
#include "AL/alc.h"
#include "AL/alut.h"
#include "SOIL.h"
#include <iostream>
#include <cstring>
#include <cassert>
#include <vector>
#include <cmath>
#include <unordered_set>
#include <unordered_map>
#include <initializer_list>
#include <functional>
#include <algorithm>

namespace art {
    using namespace std;

    /////////////////////
    //                 //
    //  Event system.  //
    //                 //
    /////////////////////

    template<typename T>
    class Listener {
    public:
        static unordered_set<Listener<T>*> LISTENERS;
        Listener() {
            LISTENERS.insert(this);
        }

        ~Listener() {
            LISTENERS.erase(this);
        }

        Listener(const Listener& other) {
            LISTENERS.insert(this);
        }

        virtual void handle(T& event) = 0;
    };

    template<typename T>
    unordered_set<Listener<T>*> Listener<T>::LISTENERS;

    template<typename T>
    void dispatch(T& event) {
        for (auto l : Listener<T>::LISTENERS) l->handle(event);
    }

    //////////////////////////////
    //                          //
    //  Vector math utilities!  //
    //                          //
    //////////////////////////////

    const double PI = 3.14159265358979323;

    double fract(double d) {
        return d - (int)d;
    }

    float fract(float f) {
        return f - (int)f;
    }

    double radians(double degrees) {
        return PI * degrees / 180.0;
    }

    double degrees(double radians) {
        return 180.0 * radians / PI;
    }

    template<int N>
    struct mat;

    template<int N>
    struct vec {
        float data[N];

        void initElement(int i, float f) {
            assert(i < N);
            data[i] = f;
        }

        template<typename ...Args>
        void initElement(int i, float f, Args... args) {
            assert(i < N);
            data[i] = f;
            initElement(i + 1, args...);
        }

        template<typename ...Args>
        vec(Args... args) {
            initElement(0, args...);
        }

        explicit vec(float f) {
            for (int i = 0; i < N; i ++) data[i] = f;
        }

        explicit vec() {
            for (int i = 0; i < N; i ++) data[i] = 0;
        }
        
        float& operator[](int i) {
            return data[i];
        }

        float operator[](int i) const {
            return data[i];
        }

        vec& operator+=(const vec& other) {
            for (int i = 0; i < N; i ++) data[i] += other[i];
            return *this;
        }

        vec& operator-=(const vec& other) {
            for (int i = 0; i < N; i ++) data[i] -= other[i];
            return *this;
        }

        vec& operator*=(const vec& other) {
            for (int i = 0; i < N; i ++) data[i] *= other[i];
            return *this;
        }

        vec& operator/=(const vec& other) {
            for (int i = 0; i < N; i ++) data[i] /= other[i];
            return *this;
        }

        vec& operator*=(float scalar) {
            for (int i = 0; i < N; i ++) data[i] *= scalar;
            return *this;
        }

        vec& operator/=(float dividend) {
            for (int i = 0; i < N; i ++) data[i] /= dividend;
            return *this;
        }

        float distsq() const {
            float sum = 0;
            for (int i = 0; i < N; i ++) sum += data[i] * data[i];
            return sum;
        }

        float dist() const {
            return sqrt(distsq());
        }

        vec normalize() const {
            return (*this) / dist();
        }

        vec& operator*=(const mat<N>& m);
    };

    template<int N>
    vec<N> operator+(vec<N> a, const vec<N>& b) { a += b; return a; }

    template<int N>
    vec<N> operator-(vec<N> a, const vec<N>& b) { a -= b; return a; }

    template<int N>
    vec<N> operator*(vec<N> a, const vec<N>& b) { a *= b; return a; }

    template<int N>
    vec<N> operator/(vec<N> a, const vec<N>& b) { a /= b; return a; }

    template<int N>
    vec<N> operator*(vec<N> a, float scalar) { a *= scalar; return a; }

    template<int N>
    vec<N> operator/(vec<N> a, float dividend) { a /= dividend; return a; }

    template<int N>
    float dot(vec<N> a, vec<N> b) {
        float sum = 0;
        for (int i = 0; i < N; i ++) sum += a[i] * b[i];
        return sum;
    }

    using vec2 = vec<2>;
    using vec3 = vec<3>;
    using vec4 = vec<4>;

    vec3 cross(vec3 a, vec3 b) {
        return { a[1] * b[2] - a[2] * b[1], 
                 a[2] * b[0] - a[0] * b[2], 
                 a[0] * b[1] - a[1] * b[0] };
    }

    template<int N>
    struct mat {
        float data[N*N];

        void initElement(int i, float f) {
            assert(i < N*N);
            data[i] = f;
        }

        template<typename ...Args>
        void initElement(int i, float f, Args... args) {
            assert(i < N*N);
            data[i] = f;
            initElement(i + 1, args...);
        }

        template<typename ...Args>
        mat(Args... args) {
            initElement(0, args...);
        }

        explicit mat(float f) {
            for (int i = 0; i < N*N; i ++) data[i] = f;
        }

        explicit mat() {
            for (int i = 0; i < N*N; i ++) data[i] = 0;
        }
    
        vec<N>& operator[](int i) {
            return *(vec<N>*)(data + i * N);
        }
    
        const vec<N>& operator[](int i) const {
            return *(const vec<N>*)(data + i * N);
        }

        mat& operator+=(const mat& other) {
            for (int i = 0; i < N * N; i ++) data[i] += other.data[i];
            return *this;
        }

        mat& operator*=(const mat& other) {
            mat prev = *this;
            for (int i = 0; i < N; i ++) {
                for (int j = 0; j < N; j ++) {
                    data[i * N + j] = 0;
                    for (int k = 0; k < N; k ++) {
                        data[i * N + j] += prev.data[i * N + k] 
                                           * other.data[k * N + j];
                    }
                }
            }
            return *this;
        }
    };

    template<int N>
    vec<N>& vec<N>::operator*=(const mat<N>& m) {
        vec<N> prev = *this;
        for (int i = 0; i < N; i ++) {
            data[i] = 0;
            for (int k = 0; k < N; k ++) {
                data[i] += prev[k] * m[i][k];
            }
        }
        return *this;
    }

    template<int N>
    mat<N> operator+(mat<N> a, const mat<N>& b) { a += b; return a; }

    template<int N>
    mat<N> operator*(mat<N> a, const mat<N>& b) { a *= b; return a; }

    template<int N>
    vec<N> operator*(vec<N> a, const mat<N>& b) { a *= b; return a; }

    using mat2 = mat<2>;
    using mat3 = mat<3>;
    using mat4 = mat<4>;

    mat4 identity() {
        return {
            1, 0, 0, 0,
            0, 1, 0, 0,
            0, 0, 1, 0,
            0, 0, 0, 1
        };
    }

    mat4 scale(float x, float y, float z) {
        return {
            x, 0, 0, 0,
            0, y, 0, 0,
            0, 0, z, 0,
            0, 0, 0, 1
        };
    }

    mat4 scale(float factor) {
        return scale(factor, factor, factor);
    }

    mat4 translate(float x, float y, float z) {
        return {
            1, 0, 0, x,
            0, 1, 0, y,
            0, 0, 1, z,
            0, 0, 0, 1
        };
    }

    mat4 xrotate(float a) {
        return {
            1, 0,      0,       0,
            0, cos(a), sin(-a), 0,
            0, sin(a), cos(a),  0,
            0, 0,      0,       1
        };
    }

    mat4 yrotate(float a) {
        return {
            cos(a),  0, sin(a), 0,
            0,       1, 0,      0,
            sin(-a), 0, cos(a), 0,
            0,       0, 0,      1
        };
    }

    mat4 zrotate(float a) {
        return {
            cos(a), sin(-a), 0, 0,
            sin(a), cos(a),  0, 0,
            0,      0,       1, 0,
            0,      0,       0, 1
        };
    }

    mat4 ortho(float left, float right, float top, float bottom, float near, float far) {
        float rl = right - left, tb = top - bottom, nf = near - far;
        return {
            2 / rl, 0,      0,       -(right + left) / rl,
            0,      2 / tb, 0,       -(top + bottom) / tb,
            0,      0,      -2 / nf, -(near + far) / tb,
            0,      0,      0,       1
        };
    }

    mat4 frustum(float left, float right, float top, float bottom, float near, float far) {
        float t1, t2, t3, t4;
        t1 = 2 * near;
        t2 = right - left;
        t3 = top - bottom;
        t4 = far - near;
		return {
            t1 / t2, 0, 0, 0,
            0, t1 / t3, 0, 0,
            (right + left) / t2, (top + bottom) / t3, (-far - near) / t4, -1,
            0, 0, (-t1 * far) / t4, 0
		};
    }

    mat4 perspective(float fov, float aspect, float near, float far) {
        float xmax, ymax;
        ymax = near * tan(radians(fov / 2));
        xmax = aspect * ymax;
        return frustum(-xmax, xmax, ymax, -ymax, near, far);
    }

    ////////////////
    //            //
    //  Shaders!  //
    //            //
    ////////////////

    class Shader;
    
    Shader* _currentShader;
    
    Shader& activeShader() {
        return *_currentShader;
    }

    struct Uniform {
    public:
        enum Type {
            FLOAT, INT, MAT4, BOUND
        };
    private:
        Type t;
        union T {
            float f;
            int i;
            mat4 m;
            T() {}
        } data;
    public:
        Uniform(float f): t(FLOAT) {
            data.f = f;
        }

        Uniform(int i): t(INT) {
            data.i = i;
        }

        Uniform(): t(BOUND) {}

        Uniform(const mat4& m): t(MAT4) {
            data.m = m;
        }

        Type type() const {
            return t;
        }

        float asFloat() const {
            return data.f;
        }

        float& asFloat() {
            return data.f;
        }

        int asInt() const {
            return data.i;
        }

        int& asInt() {
            return data.i;
        }

        mat4& asMatrix() {
            return data.m;
        }

        const mat4& asMatrix() const {
            return data.m;
        }
    };

    class Shader {
        GLuint id;
        unordered_map<string, GLint> uniforms;
        GLint findUniform(const string& name) {
            auto it = uniforms.find(name);
            if (it == uniforms.end()) {
                GLint loc = glGetUniformLocation(id, name.c_str());
                printf("found %s at %u for shader %u\n", name.c_str(), loc, id);
                uniforms.emplace(name, loc);
                return loc;
            }
            return it->second;
        }
    public:
        Shader(): id(0) {}
        Shader(GLuint id_in): id(id_in) {}

        Shader& with(const string& uniform, int i) {
            glUniform1i(findUniform(uniform), i);
            return *this;
        }

        Shader& with(const string& uniform, float x) {
            glUniform1f(findUniform(uniform), x);
            return *this;
        }

        Shader& with(const string& uniform, float x, float y) {
            glUniform2f(findUniform(uniform), x, y);
            return *this;
        }

        Shader& with(const string& uniform, vec2 xy) {
            return with(uniform, xy[0], xy[1]);
        }

        Shader& with(const string& uniform, float x, float y, float z) {
            glUniform3f(findUniform(uniform), x, y, z);
            return *this;
        }

        Shader& with(const string& uniform, vec3 xyz) {
            return with(uniform, xyz[0], xyz[1], xyz[2]);
        }

        Shader& with(const string& uniform, float x, float y, 
                     float z, float w) {
            glUniform4f(findUniform(uniform), x, y, z, w);
            return *this;
        }

        Shader& with(const string& uniform, vec4 xyzw) {
            return with(uniform, xyzw[0], xyzw[1], xyzw[2], xyzw[3]);
        }

        Shader& with(const string& uniform, const mat4& mat) {
            glUniformMatrix4fv(findUniform(uniform), 1, GL_TRUE, 
                               (const GLfloat*)&mat);
            return *this;
        }

        Shader& bind() {
            glUseProgram(id);
            _currentShader = this;
            return *this;
        }

        void unbind() {
            glUseProgram(0);
            _currentShader = nullptr;
        }
    };

    namespace white {
        const char* vs = R"(
            #version 330
            layout(location=0) in vec3 pos;

            uniform mat4 projection;
            uniform mat4 view;
            uniform mat4 transform;

            void main() {
                gl_Position = projection * transform * vec4(pos, 1);
            }
        )";

        const char* fs = R"(
            #version 330
            out vec4 color;

            void main() {
                color = vec4(1);
            }
        )";

        Shader shader;
    }

    namespace passthrough {
        const char* vs = R"(
            #version 330
            layout(location=0) in vec3 pos;
            layout(location=1) in vec4 col;
            layout(location=2) in vec2 uv;
            // layout(location=3) in vec4 spr;
            // layout(location=4) in vec3 norm;

            out vec4 v_col;
            out vec2 v_uv;
            // out vec4 v_spr;

            void main() {
                gl_Position = vec4(pos, 1);
                v_col = col;
                v_uv = uv;
                // v_spr = spr;
            }
        )";

        const char* fs = R"(
            #version 330
            in vec4 v_col;
            in vec2 v_uv;
            // in vec4 v_spr;

            uniform sampler2D tex;

            out vec4 color;

            void main() {
                color = v_col * texture2D(tex, v_uv);
            }
        )";

        Shader shader;
    }

    namespace depth {
        const char* vs = R"(
            #version 330
            layout(location=0) in vec3 pos;
            layout(location=1) in vec4 col;
            layout(location=2) in vec2 uv;
            layout(location=3) in vec4 spr;
            layout(location=4) in vec3 norm;

            out vec4 v_col;
            out vec2 v_uv;
            out vec4 v_spr;

            uniform mat4 projection;
            uniform mat4 view;
            uniform mat4 transform;

            void main() {
                gl_Position = projection * view * transform * vec4(pos, 1);
                v_uv = uv;
                v_spr = spr;
                v_col = col;
            }
        )";

        const char* fs = R"(
            #version 330
            in vec4 v_col;
            in vec2 v_uv;
            in vec4 v_spr;

            uniform sampler2D tex;
            uniform vec2 texdim;

            out vec4 color;

            void main() {
                vec2 suv = vec2(v_spr.x + fract(v_uv.x) * v_spr.z, v_spr.y + fract(v_uv.y) * v_spr.w);
                suv /= texdim;
                vec4 pix = v_col * texture2D(tex, suv);
                if (pix.a < 0.99) discard;
                float depth = gl_FragCoord.z / gl_FragCoord.w;
                color = vec4(vec3(depth), 1.0);
            }
        )";

        Shader shader;
    }

    namespace shadow {
        const char* vs = R"(
            #version 330
            layout(location=0) in vec3 pos;
            layout(location=1) in vec4 col;
            layout(location=2) in vec2 uv;
            layout(location=3) in vec4 spr;
            layout(location=4) in vec3 norm;

            out vec4 o_pos;
            out vec4 o_col;
            out vec4 o_spr;
            out vec2 o_uv;
            out float bias;

            uniform mat4 o_projection;
            uniform mat4 o_view;
            uniform mat4 o_transform;

            uniform mat4 projection;
            uniform mat4 view;
            uniform mat4 transform;
            uniform vec3 light;

            void main() {
                vec3 normal = normalize(mat3(transform) * norm);
                float diff = 1 - abs(dot(normal, light));
                gl_Position = projection * view * transform * vec4(pos, 1);
                o_pos = (o_projection * o_view * transform * vec4(pos + ((0.01 + 0.99 * diff) * norm), 1) + vec4(1.0)) / 2.0;
                o_col = col;
                o_uv = uv;
                o_spr = spr;
                bias = 0.0002;
            }
        )";

        const char* fs = R"(
            #version 330
            in vec4 o_pos;
            in vec4 o_col;
            in vec4 o_spr;
            in vec2 o_uv;
            in float bias;

            uniform sampler2D tex;
            uniform sampler2D depthbuf;
            uniform int width, height;
            uniform vec2 texdim;

            out vec4 color;
            
            vec2 rand(vec2 co){
                return vec2(
                    fract(sin(dot(co.xy, vec2(12021.9898,75893.233))) * 4313016.54531937),
                    fract(cos(dot(co.yx, vec2(54370.1204,31174.8922))) * 6333703.92404723)
                );
            }

            void main() {
                vec2 suv = vec2(o_spr.x + fract(o_uv.x) * o_spr.z, o_spr.y + fract(o_uv.y) * o_spr.w);
                suv /= texdim;
                vec4 pix = o_col * texture2D(tex, suv);
                if (pix.a < 0.99) discard;
                
                float depth = o_pos.z / o_pos.w;

                float count = 0;
                color = vec4(0);
                for (float i = -3; i < 4; i ++) {
                    for (float j = -3; j < 4; j ++) {
                        vec2 off = 1.5 * vec2(i, j) / vec2(width, height);
                        vec2 pos = o_pos.xy + off;
                        vec4 pix = texture2D(depthbuf, pos.xy);
                        if (pix.r == 0) continue;
                        if (pos.x > 0 && pos.x < 1 && pos.y > 0 && pos.y < 1) {
                            if (depth > pix.r + bias) {
                                color += vec4(0, 0, 0, 1.0);
                            }
                            else color += vec4(1.0);
                            count ++;
                        }
                    }
                }
                // if (count > 24) color = vec4(0.5, 0.5, 0.5, 1.0);
                // else color = vec4(1.0);
                color /= count;
                // color = pix;
            }
        )";

        Shader shader;
    }

    namespace light {
        const char* vs = R"(
            #version 330
            layout(location=0) in vec3 pos;
            layout(location=1) in vec4 col;
            layout(location=2) in vec2 uv;
            layout(location=3) in vec4 spr;
            layout(location=4) in vec3 norm;

            out vec4 v_pos, w_pos;
            out vec3 v_norm;
            out vec4 v_col, v_spr;
            out vec2 v_uv;

            uniform mat4 projection;
            uniform mat4 view;
            uniform mat4 transform;

            void main() {
                v_norm = normalize(mat3(transform) * norm);
                gl_Position = projection * view * transform * vec4(pos, 1);
                w_pos = transform * vec4(pos, 1);
                v_pos = projection * view * transform * vec4(pos, 1);
                v_col = col;
                v_spr = spr;
                v_uv = uv;
            }
        )";

        const char* fs = R"(
            #version 330
            in vec4 v_pos, w_pos;
            in vec4 v_col, v_spr;
            in vec2 v_uv;
            in vec3 v_norm;

            struct Light {
                vec3 pos;
                vec4 col;
                float radius;
            };

            uniform Light lights[32];
            uniform sampler2D tex;
            uniform vec2 texdim;

            out vec4 color;

            void main() {
                vec2 suv = vec2(v_spr.x + fract(v_uv.x) * v_spr.z, v_spr.y + fract(v_uv.y) * v_spr.w);
                suv /= texdim;
                vec4 pix = v_col * texture2D(tex, suv);
                if (pix.a < 0.01) discard;

                vec3 light = vec3(0);
                for (int i = 0; i < 32; i ++) {
                    vec3 v = normalize(lights[i].pos - w_pos.xyz);
                    float n = (dot(v, v_norm) + 2.0) / 3.0;
                    float d = (lights[i].radius - distance(w_pos.xyz, lights[i].pos)) / lights[i].radius;
                    d = clamp(d, 0, 1);
                    // d = round(d * 32.0) / 32.0;
                    d = d * d;
                    light += lights[i].col.rgb * lights[i].col.a * d * n;
                }
                color = vec4(light.r, light.g, light.b, pix.a);
            }
        )";

        Shader shader;
    }

    namespace normal {
        const char* vs = R"(
            #version 330
            layout(location=0) in vec3 pos;
            layout(location=1) in vec4 col;
            layout(location=2) in vec2 uv;
            layout(location=3) in vec4 spr;
            layout(location=4) in vec3 norm;

            out vec4 v_pos;
            out vec4 v_col;
            out vec2 v_uv;
            out vec4 v_spr;
            out vec3 v_norm;
            out float litness;

            uniform mat4 projection;
            uniform mat4 view;
            uniform mat4 transform;
            uniform vec3 light;
            uniform float light_intensity;

            void main() {
                v_pos = projection * view * transform * vec4(pos, 1);
                gl_Position = v_pos;
                litness = 0.1 + (1.0 + dot(normalize(mat3(transform) * norm), light)) / 2.0;
                litness *= light_intensity;
                v_col = col;
                v_uv = uv;
                v_spr = spr;
                v_norm = norm;
            }
        )";

        const char* fs = R"(
            #version 330
            in vec4 v_pos;
            in vec4 v_col;
            in vec2 v_uv;
            in vec4 v_spr;
            in vec3 v_norm;
            in float litness;

            uniform int width, height;
            uniform sampler2D tex;
            uniform sampler2D shadows;
            uniform sampler2D lights;
            uniform vec2 texdim;

            out vec4 color;

            void main() {
                vec2 suv = vec2(v_spr.x + 0.1 + fract(v_uv.x) * (v_spr.z - 0.2), v_spr.y + 0.1 + fract(v_uv.y) * (v_spr.w - 0.2));
                suv /= texdim;
                vec4 pix = texture2D(tex, suv);
                if (v_norm.x * v_norm.x + v_norm.y * v_norm.y + v_norm.z * v_norm.z < 0.01) {
                    color = v_col * pix;
                    return;
                }
                if (pix.a < 0.01) discard;
                
                float shade = 0;
                if (pix.a * v_col.a > 0.99) {
                    float count = 1;
                    for (float i = -2; i < 3; i ++) {
                        for (float j = -2; j < 3; j ++) {
                            vec4 spix = texture2D(shadows, vec2(0.5, 0.5) + (v_pos.xy + vec2(0.625 * i / width, 0.75 * j / height)) * 0.25);
                            if (spix.a >= 0.99) {
                                shade += spix.r;
                                count ++;
                            }
                        }
                    }
                    shade /= count;
                }
                else shade = 1;
                shade = 1 - ((1 - shade) * 0.5);
                float brightness = litness * shade;
                // brightness = round(brightness * 32.0) / 32.0;
                vec4 addedlight = vec4(texture2D(lights, (gl_FragCoord.xy) / vec2(width, height)).rgb, 0);
                
                brightness += (addedlight.r + addedlight.g + addedlight.b) / 6;

                color = vec4(vec3(brightness * v_col.rgb), v_col.a) * pix + addedlight;
                if (brightness < 0.65) {
                    color.r *= 1 - ((1 - (brightness + 0.35)) / 2);
                    color.g *= 1 - ((1 - (brightness + 0.35)) / 4);
                }
            }
        )";

        Shader shader;
    }

    namespace unlit {
        const char* vs = R"(
            #version 330
            layout(location=0) in vec3 pos;
            layout(location=1) in vec4 col;
            layout(location=2) in vec2 uv;
            layout(location=3) in vec4 spr;
            layout(location=4) in vec3 norm;

            out vec4 v_pos;
            out vec4 v_col;
            out vec2 v_uv;
            out vec4 v_spr;

            uniform mat4 projection;
            uniform mat4 view;
            uniform mat4 transform;

            void main() {
                v_pos = projection * view * transform * vec4(pos, 1);
                gl_Position = v_pos;
                v_col = col;
                v_uv = uv;
                v_spr = spr;
            }
        )";

        const char* fs = R"(
            #version 330
            in vec4 v_pos;
            in vec4 v_col;
            in vec2 v_uv;
            in vec4 v_spr;

            uniform int width, height;
            uniform sampler2D tex;
            uniform vec2 texdim;

            out vec4 color;

            void main() {
                vec2 suv = vec2(v_spr.x + 0.1 + fract(v_uv.x) * (v_spr.z - 0.2), v_spr.y + 0.1 + fract(v_uv.y) * (v_spr.w - 0.2));
                suv /= texdim;
                color = v_col * texture2D(tex, suv);
                if (color.a < 0.01) discard;
            }
        )";

        Shader shader;
    }

    /////////////////
    //             //
    //  Textures!  //
    //             //
    /////////////////

    GLuint toRGB32(float r, float g, float b, float a) {
        GLuint ir = (int)(r * 255), ig = (int)(g * 255),
               ib = (int)(b * 255), ia = (int)(a * 255);
        return (ia << 24) | (ib << 16) | (ig << 8) | ir;
    }

    class Texture {
        GLuint id;
        int w, h;
        bool dirty, _translucent = false, _additive = false;
        GLuint* data;

        void free() {
            if (id) glDeleteTextures(1, &id);
            if (data) delete[] data;
        }

        void init() {
            glGenTextures(1, &id);
            
            glBindTexture(GL_TEXTURE_2D, id);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, 
                         GL_UNSIGNED_BYTE, data);
            glBindTexture(GL_TEXTURE_2D, 0);
        }

        void update() {
            glBindTexture(GL_TEXTURE_2D, id);
            glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, w, h, GL_RGBA, GL_UNSIGNED_BYTE, data);
            glBindTexture(GL_TEXTURE_2D, 0);
            dirty = false;
        }
    public:
        Texture(int width, int height, vec4 color = {1, 1, 1, 1}):
            w(width), h(height), dirty(false), data(new GLuint[w * h]) {
            GLuint c = toRGB32(color[0], color[1], color[2], color[3]);
            for (int i = 0; i < w * h; i ++) {
                data[i] = c;
            }
            init();
        }

        Texture(const char* path) {
            int channels;
            unsigned char* img = SOIL_load_image(path, &w, &h, &channels, SOIL_LOAD_RGBA);
            printf("wxh channels = %d %d %d\n", w, h, channels);
            data = new GLuint[w * h];
            for (int i = 0; i < w * h; i ++) {
                data[i] = ((GLuint*)img)[i];
            }
            init();
            SOIL_free_image_data(img);
        }

        ~Texture() {
            free();
        }

        Texture(const Texture& other):
            w(other.w), h(other.h), dirty(false), data(new GLuint[w * h]) {
            for (int i = 0; i < w * h; i ++) {
                data[i] = other.data[i];
            }
            update();
        }

        Texture& operator=(const Texture& other) {
            if (this != &other) {
                free();
                w = other.w, h = other.h;
                data = new GLuint[w * h];
                for (int i = 0; i < w * h; i ++) {
                    data[i] = other.data[i];
                }
                update();
            }
            return *this;
        }

        void setAdditive(bool additive) {
            _additive = additive;
        }

        void setTranslucent(bool translucent) {
            _translucent = translucent;
        }

        bool additive() const {
            return _additive;
        }

        bool translucent() const {
            return _translucent;
        }

        void set(int x, int y, float r, float g, float b, float a) {
            int i = y * h + x;
            GLuint color = toRGB32(r, g, b, a);
            data[i] = color, dirty = true;
        }

        void set(int x, int y, vec4 color) {
            set(x, y, color[0], color[1], color[2], color[3]);
        }

        vec4 get(int x, int y) const {
            GLuint color = data[y * h + x];
            float r, g, b, a;
            r = ((color) & 255) / 255.0f;
            g = ((color >> 8) & 255) / 255.0f;
            b = ((color >> 16) & 255) / 255.0f;
            a = ((color >> 24) & 255) / 255.0f;
            return { r, g, b, a };
        }

        void bind() {
            if (dirty) update();
            activeShader().with("texdim", w, h);
            glBindTexture(GL_TEXTURE_2D, id);
        }

        void unbind() {
            glBindTexture(GL_TEXTURE_2D, 0);
        }
    };

    //////////////////////////////
    //                          //
    //  Library loading stuff.  //
    //                          //
    //////////////////////////////

    Shader loadShader(const char* vs, const char* fs) {
        GLuint vsh = glCreateShader(GL_VERTEX_SHADER);
        GLuint fsh = glCreateShader(GL_FRAGMENT_SHADER);

        GLint size = strlen(vs);
        glShaderSource(vsh, 1, &vs, &size);
        size = strlen(fs);
        glShaderSource(fsh, 1, &fs, &size);

        char log[1024];
        GLsizei length = 0;
        GLint status = GL_TRUE;
        glCompileShader(vsh);
        if (glGetShaderiv(vsh, GL_COMPILE_STATUS, &status), !status) {
            glGetShaderInfoLog(vsh, 1024, &length, log);
            printf("VERTEX SHADER ERROR:\n%s\n", log);
        }
        glCompileShader(fsh);
        if (glGetShaderiv(fsh, GL_COMPILE_STATUS, &status), !status) {
            glGetShaderInfoLog(fsh, 1024, &length, log);
            printf("FRAGMENT SHADER ERROR:\n%s\n", log);
        }

        GLuint shader = glCreateProgram();
        glAttachShader(shader, vsh);
        glAttachShader(shader, fsh);
        glLinkProgram(shader);
        return Shader(shader);
    }

    void glfwErrors(int length, const char* msg) {
        cout << "[GLFW ERROR] " << msg << endl;
    }

    GLuint vao;

    void windowSizeCallback(GLFWwindow* win, int width, int height);

    ALCdevice* aldevice;
    ALCcontext* alctx;

    void init() {
        glfwInit();
        glfwSetErrorCallback(glfwErrors);
        alutInit(nullptr, nullptr);
        // aldevice = alcOpenDevice(nullptr);
        // alctx = alcCreateContext(aldevice, nullptr);
        // alcMakeContextCurrent(alctx);
        alListener3f(AL_POSITION, 0, 0, 0);
        alListener3f(AL_VELOCITY, 0, 0, 0);
        ALfloat ori[] = { 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f };
        alListenerfv(AL_ORIENTATION, ori);
    }

    void initGL() {
        passthrough::shader = loadShader(passthrough::vs, passthrough::fs);
        white::shader = loadShader(white::vs, white::fs);
        normal::shader = loadShader(normal::vs, normal::fs);
        depth::shader = loadShader(depth::vs, depth::fs);
        shadow::shader = loadShader(shadow::vs, shadow::fs);
        light::shader = loadShader(light::vs, light::fs);
        unlit::shader = loadShader(unlit::vs, unlit::fs);
        glEnable(GL_CULL_FACE);
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_TEXTURE_2D);
        glGenVertexArrays(1, &vao);
        glBindVertexArray(vao);
        glEnable(GL_BLEND);
        glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
    }

    void free() {
        glDeleteVertexArrays(1, &vao);
        glfwTerminate();
    }

    ///////////////////////
    //                   //
    //  Sound and Music  //
    //                   //
    ///////////////////////

    class Sound {
        ALuint source, buffer;
    public:
        Sound(const char* path) {
            alGenSources(1, &source);
            alSourcef(source, AL_PITCH, 1);
            alSourcef(source, AL_GAIN, 1);
            alSource3f(source, AL_POSITION, 0, 0, 0);
            alSource3f(source, AL_VELOCITY, 0, 0, 0);
            alSourcei(source, AL_LOOPING, AL_FALSE);
            buffer = alutCreateBufferFromFile(path);
            alSourcei(source, AL_BUFFER, buffer);
        }

        void play(float volume = 1.0f, float pitch = 1.0f) const {
            alSourcef(source, AL_PITCH, pitch);
            alSourcef(source, AL_GAIN, volume);
            alSourcePlay(source);
        }

        float getVolume() const {
            float volume;
            alGetSourcef(source, AL_GAIN, &volume);
            return volume;
        }

        void setVolume(float f) {
            alSourcef(source, AL_GAIN, f);
        }

        float getPitch() const {
            float pitch;
            alGetSourcef(source, AL_PITCH, &pitch);
            return pitch;
        }

        void setPitch(float f) {
            alSourcef(source, AL_PITCH, f);
        }
    };

    class Music {
        ALuint source, buffer;
    public:
        Music(const char* path) {
            alGenSources(1, &source);
            alSourcef(source, AL_PITCH, 1);
            alSourcef(source, AL_GAIN, 1);
            alSource3f(source, AL_POSITION, 0, 0, 0);
            alSource3f(source, AL_VELOCITY, 0, 0, 0);
            alSourcei(source, AL_LOOPING, AL_TRUE);
            buffer = alutCreateBufferFromFile(path);
            alSourcei(source, AL_BUFFER, buffer);
        }

        void play(float volume = 1.0f, float pitch = 1.0f) const {
            alSourcef(source, AL_PITCH, pitch);
            alSourcef(source, AL_GAIN, volume);
            alSourcePlay(source);
        }

        void stop() const {
            alSourceStop(source);
        }

        float getVolume() const {
            float volume;
            alGetSourcef(source, AL_GAIN, &volume);
            return volume;
        }

        void setVolume(float f) {
            alSourcef(source, AL_GAIN, f);
        }

        float getPitch() const {
            float pitch;
            alGetSourcef(source, AL_PITCH, &pitch);
            return pitch;
        }

        void setPitch(float f) {
            alSourcef(source, AL_PITCH, f);
        }
    };

    /////////////////////////////////////////////////////
    //                                                 //
    //  Sketches!                                      //
    //  ---------------------------------------------  //
    //  Sketches are bits of geometry and model data.  //
    //  They can be given new vertices and colors and  //
    //  such, and will automatically rebake their      //
    //  models when drawn.                             //
    //                                                 //
    /////////////////////////////////////////////////////

    class Sketch {
        int count;
        vector<float> vertices;
        vector<float> colors;   
        vector<float> texcoords;
        vector<float> sprites;
        vector<float> normals;
        GLuint buffers[5];
        GLuint bufsizes[5];
        bool dirty;

        static constexpr const int VERTEX_ATTRIB = 0;
        static constexpr const int COLOR_ATTRIB = 1;
        static constexpr const int TEXCOORD_ATTRIB = 2;
        static constexpr const int SPRITE_ATTRIB = 3;
        static constexpr const int NORMAL_ATTRIB = 4;

        void bakeAttrib(int attrib, const vector<float>& data) {
            glBindBuffer(GL_ARRAY_BUFFER, buffers[attrib]);
            if (bufsizes[attrib] < sizeof(float) * data.size()) {
                glBufferData(GL_ARRAY_BUFFER, sizeof(float) * data.size(), &data[0], GL_DYNAMIC_DRAW);
                bufsizes[attrib] = sizeof(float) * data.size();
            }
            else {
                glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(float) * data.size(), &data[0]);
            }
            glBindBuffer(GL_ARRAY_BUFFER, 0);
        }

        void bake() {
            bakeAttrib(VERTEX_ATTRIB, vertices);
            bakeAttrib(COLOR_ATTRIB, colors);
            bakeAttrib(TEXCOORD_ATTRIB, texcoords);
            bakeAttrib(SPRITE_ATTRIB, sprites);
            bakeAttrib(NORMAL_ATTRIB, normals);
            dirty = false;
        }

        void enableAttribs() {
            glEnableVertexAttribArray(VERTEX_ATTRIB);
            glEnableVertexAttribArray(COLOR_ATTRIB);
            glEnableVertexAttribArray(TEXCOORD_ATTRIB);
            glEnableVertexAttribArray(SPRITE_ATTRIB);
            glEnableVertexAttribArray(NORMAL_ATTRIB);
        }

        void disableAttribs() {
            glDisableVertexAttribArray(VERTEX_ATTRIB);
            glDisableVertexAttribArray(COLOR_ATTRIB);
            glDisableVertexAttribArray(TEXCOORD_ATTRIB);
            glDisableVertexAttribArray(SPRITE_ATTRIB);
            glDisableVertexAttribArray(NORMAL_ATTRIB);
        }

        void bindAttrib(int attrib, int size) {
            glBindBuffer(GL_ARRAY_BUFFER, buffers[attrib]);
            glVertexAttribPointer(attrib, size, GL_FLOAT, GL_FALSE, 0, 0);
            glBindBuffer(GL_ARRAY_BUFFER, 0);
        }

    public:
        Sketch(): count(0), dirty(true) {
            for (int i = 0; i < 5; i ++) bufsizes[i] = 0;
            glGenBuffers(5, buffers);
        }

        ~Sketch() {
            glDeleteBuffers(5, buffers);
        }

        Sketch(const Sketch& other): Sketch() {
            count = other.count;
            vertices = other.vertices;
            colors = other.colors;
            texcoords = other.texcoords;
            sprites = other.sprites;
            normals = other.normals;
        }

        Sketch& operator=(const Sketch& other) {
            if (this != &other) {
                glDeleteBuffers(5, buffers);
                dirty = true;
                glGenBuffers(5, buffers);
                for (int i = 0; i < 5; i ++) bufsizes[i] = 0;
                count = other.count;
                vertices = other.vertices;
                colors = other.colors;
                texcoords = other.texcoords;
                sprites = other.sprites;
                normals = other.normals;
            }
            return *this;
        }

        void clear() {
            vertices.clear();
            colors.clear();
            texcoords.clear();
            sprites.clear();
            normals.clear();
            count = 0;
            dirty = true;
        }

        void vertex(float x, float y, float z) {
            ++ count;
            vertices.push_back(x);
            vertices.push_back(y);
            vertices.push_back(z);
            dirty = true;
        }

        void color(float r, float g, float b, float a) {
            colors.push_back(r);
            colors.push_back(g);
            colors.push_back(b);
            colors.push_back(a);
            dirty = true;
        }

        void texture(float u, float v) {
            texcoords.push_back(u);
            texcoords.push_back(v);
            dirty = true;
        }

        void sprite(float sx, float sy, float sw, float sh) {
            sprites.push_back(sx);
            sprites.push_back(sy);
            sprites.push_back(sw);
            sprites.push_back(sh);
            dirty = true;
        }

        void normal(float nx, float ny, float nz) {
            normals.push_back(nx);
            normals.push_back(ny);
            normals.push_back(nz);
            dirty = true;
        }

        void vertex(vec3 point) {
            vertex(point[0], point[1], point[2]);
        }

        void color(vec4 col) {
            color(col[0], col[1], col[2], col[3]);
        }

        void texture(vec2 texcoord) {
            texture(texcoord[0], texcoord[1]);
        }

        void sprite(vec4 spr) {
            sprite(spr[0], spr[1], spr[2], spr[3]);
        }

        void normal(vec3 norm) {
            normal(norm[0], norm[1], norm[2]);
        }

        void draw() {
            if (dirty) bake();
            enableAttribs();

            bindAttrib(VERTEX_ATTRIB, 3);
            bindAttrib(COLOR_ATTRIB, 4);
            bindAttrib(TEXCOORD_ATTRIB, 2);
            bindAttrib(SPRITE_ATTRIB, 4);
            bindAttrib(NORMAL_ATTRIB, 3);
            glDrawArrays(GL_TRIANGLES, 0, count);

            disableAttribs();
        }
    };

    namespace draw {
        int TILE_WIDTH = 32;
        vec4 _color = { 1, 1, 1, 1 };
        vec4 _sprite = { 0, 0, 1, 1 };
        vec4 _topsprite = { 0, 0, 1, 1 };
        vec4 _sidesprite = { 0, 0, 1, 1 };
        vec4 _bottomsprite = { 0, 0, 1, 1 };
        vec4 _westsprite = { 0, 0, 1, 1 };
        vec4 _eastsprite = { 0, 0, 1, 1 };
        vec4 _northsprite = { 0, 0, 1, 1 };
        vec4 _southsprite = { 0, 0, 1, 1 };
        vec4 _uv = { 0, 0, 1, 1 };
        bool _autouv = true;
        bool _stretch = false;
        bool _unlit = false;

        void setUnlit(bool b) {
            _unlit = b;
        }

        void setStretch(bool b) {
            _autouv = !b;
        }

        void uv() {
            _uv = { 0, 0, 1, 1 };
            _autouv = true;
        }

        void uv(float u, float v, float w, float h) {
            _uv = { u, v, w, h };
            _autouv = false;
        }

        void color() {
            _color = { 1, 1, 1, 1 };
        }

        void color(float r, float g, float b, float a) {
            _color = { r, g, b, a };
        }

        void color(vec4 c) {
            _color = c;
        }

        void sprite() {
            _westsprite = _eastsprite = _northsprite = _southsprite = 
            _topsprite = _sidesprite = _bottomsprite = _sprite = { 0, 0, 1, 1 };
        }

        void sprite(float x, float y, float w, float h) {
            _westsprite = _eastsprite = _northsprite = _southsprite = 
            _topsprite = _sidesprite = _bottomsprite = _sprite = { x, y, w, h };
        }

        void sprite(vec4 s) {
            _westsprite = _eastsprite = _northsprite = _southsprite = 
            _topsprite = _sidesprite = _bottomsprite = _sprite = s;
        }

        void topsprite(float x, float y, float w, float h) {
            _topsprite = { x, y, w, h };   
        }

        void topsprite(vec4 s) {
            _topsprite = s;
        }

        void sidesprite(float x, float y, float w, float h) {
            _westsprite = _eastsprite = _northsprite = _southsprite = 
            _sidesprite = { x, y, w, h };   
        }

        void sidesprite(vec4 s) {
            _westsprite = _eastsprite = _northsprite = _southsprite = 
            _sidesprite = s;
        }

        void bottomsprite(float x, float y, float w, float h) {
            _bottomsprite = { x, y, w, h };   
        }

        void bottomsprite(vec4 s) {
            _bottomsprite = s;
        }

        void westsprite(float x, float y, float w, float h) {
            _westsprite = { x, y, w, h };
        }

        void westsprite(vec4 s) {
            _westsprite = s;
        }

        void eastsprite(float x, float y, float w, float h) {
            _eastsprite = { x, y, w, h };
        }

        void eastsprite(vec4 s) {
            _eastsprite = s;
        }

        void northsprite(float x, float y, float w, float h) {
            _northsprite = { x, y, w, h };
        }

        void northsprite(vec4 s) {
            _northsprite = s;
        }

        void southsprite(float x, float y, float w, float h) {
            _southsprite = { x, y, w, h };
        }

        void southsprite(vec4 s) {
            _southsprite = s;
        }

        void rect(Sketch& s, float x, float y, float w, float h, float z = 0) {
            s.vertex(x, y, z); s.texture(_uv[0], _uv[1]);
            s.vertex(x + w, y, z); s.texture(_uv[0] + _uv[2], _uv[1]);
            s.vertex(x + w, y + h, z); s.texture(_uv[0] + _uv[2], _uv[1] + _uv[3]);
            s.vertex(x + w, y + h, z); s.texture(_uv[0] + _uv[2], _uv[1] + _uv[3]);
            s.vertex(x, y + h, z); s.texture(_uv[0], _uv[1] + _uv[3]);
            s.vertex(x, y, z); s.texture(_uv[0], _uv[1]);

            for (int i = 0; i < 6; i ++) s.color(_color);
            for (int i = 0; i < 6; i ++) s.sprite(_sprite);
            for (int i = 0; i < 6; i ++) s.normal(0, 0, -1);
        }

        void sprite(Sketch& s, float x, float y, vec4 spr) {
            sprite(spr);
            rect(s, x, y, spr[2], spr[3]);
            sprite();
        }

        void segm(Sketch& s, float x, float y, float z, float w, float h, float l, float r1, float r2) {
            if (!_autouv) for (int i = 0; i < 6; i ++) {
                s.texture(_uv[0], _uv[1]);
                s.texture(_uv[0] + _uv[2], _uv[1]);
                s.texture(_uv[0] + _uv[2], _uv[1] + _uv[3]);
                s.texture(_uv[0] + _uv[2], _uv[1] + _uv[3]);
                s.texture(_uv[0], _uv[1] + _uv[3]);
                s.texture(_uv[0], _uv[1]);
            }
            else {
                for (int i = 0; i < 2; i ++) {
                    s.texture(0, 0);
                    s.texture(l / TILE_WIDTH, 0);
                    s.texture(l / TILE_WIDTH, h / TILE_WIDTH);
                    s.texture(l / TILE_WIDTH, h / TILE_WIDTH);
                    s.texture(0, h / TILE_WIDTH);
                    s.texture(0, 0);
                }
                
                for (int i = 0; i < 2; i ++) {
                    s.texture(0, 0);
                    s.texture(w / TILE_WIDTH, 0);
                    s.texture(w / TILE_WIDTH, l / TILE_WIDTH);
                    s.texture(w / TILE_WIDTH, l / TILE_WIDTH);
                    s.texture(0, l / TILE_WIDTH);
                    s.texture(0, 0);
                }
                
                for (int i = 0; i < 2; i ++) {
                    s.texture(0, 0);
                    s.texture(w / TILE_WIDTH, 0);
                    s.texture(w / TILE_WIDTH, h / TILE_WIDTH);
                    s.texture(w / TILE_WIDTH, h / TILE_WIDTH);
                    s.texture(0, h / TILE_WIDTH);
                    s.texture(0, 0);
                }
            }

            float cx = x + w / 2, cz = z + l / 2;
            float dx1 = r1 * w / 2, dz1 = r1 * l / 2;
            float dx2 = r2 * w / 2, dz2 = r2 * l / 2;

            s.vertex(cx - dx2, y + h, cz - dz2);
            s.vertex(cx - dx2, y + h, cz + dz2);
            s.vertex(cx - dx1, y, cz + dz1);
            s.vertex(cx - dx1, y, cz + dz1);
            s.vertex(cx - dx1, y, cz - dz1);
            s.vertex(cx - dx2, y + h, cz - dz2);

            s.vertex(cx + dx2, y + h, cz + dz2);
            s.vertex(cx + dx2, y + h, cz - dz2);
            s.vertex(cx + dx1, y, cz - dz1);
            s.vertex(cx + dx1, y, cz - dz1);
            s.vertex(cx + dx1, y, cz + dz1);
            s.vertex(cx + dx2, y + h, cz + dz2);

            s.vertex(cx + dx1, y, cz - dz1);
            s.vertex(cx - dx1, y, cz - dz1);
            s.vertex(cx - dx1, y, cz + dz1);
            s.vertex(cx - dx1, y, cz + dz1);
            s.vertex(cx + dx1, y, cz + dz1);
            s.vertex(cx + dx1, y, cz - dz1);
            
            s.vertex(cx - dx2, y + h, cz - dz2);
            s.vertex(cx + dx2, y + h, cz - dz2);
            s.vertex(cx + dx2, y + h, cz + dz2);
            s.vertex(cx + dx2, y + h, cz + dz2);
            s.vertex(cx - dx2, y + h, cz + dz2);
            s.vertex(cx - dx2, y + h, cz - dz2);

            s.vertex(cx + dx2, y + h, cz - dz2);
            s.vertex(cx - dx2, y + h, cz - dz2);
            s.vertex(cx - dx1, y, cz - dz1);
            s.vertex(cx - dx1, y, cz - dz1);
            s.vertex(cx + dx1, y, cz - dz1);
            s.vertex(cx + dx2, y + h, cz - dz2);

            s.vertex(cx - dx2, y + h, cz + dz2);
            s.vertex(cx + dx2, y + h, cz + dz2);
            s.vertex(cx + dx1, y, cz + dz1);
            s.vertex(cx + dx1, y, cz + dz1);
            s.vertex(cx - dx1, y, cz + dz1);
            s.vertex(cx - dx2, y + h, cz + dz2);
            
            for (int i = 0; i < 36; i ++) s.color(_color);

            for (int i = 0; i < 6; i ++) s.sprite(_westsprite);
            for (int i = 0; i < 6; i ++) s.sprite(_eastsprite);
            for (int i = 0; i < 6; i ++) s.sprite(_bottomsprite);
            for (int i = 0; i < 6; i ++) s.sprite(_topsprite);
            for (int i = 0; i < 6; i ++) s.sprite(_northsprite);
            for (int i = 0; i < 6; i ++) s.sprite(_southsprite);

            for (int i = 0; i < 6; i ++) s.normal(-1, 0, 0);
            for (int i = 0; i < 6; i ++) s.normal(1, 0, 0);
            for (int i = 0; i < 6; i ++) s.normal(0, -1, 0);
            for (int i = 0; i < 6; i ++) s.normal(0, 1, 0);
            for (int i = 0; i < 6; i ++) s.normal(0, 0, -1);
            for (int i = 0; i < 6; i ++) s.normal(0, 0, 1);
        }

        void cube(Sketch& s, float x, float y, float z, float w, float h, float l) {
            if (!_autouv) for (int i = 0; i < 6; i ++) {
                s.texture(_uv[0], _uv[1]);
                s.texture(_uv[0] + _uv[2], _uv[1]);
                s.texture(_uv[0] + _uv[2], _uv[1] + _uv[3]);
                s.texture(_uv[0] + _uv[2], _uv[1] + _uv[3]);
                s.texture(_uv[0], _uv[1] + _uv[3]);
                s.texture(_uv[0], _uv[1]);
            }
            else {
                for (int i = 0; i < 2; i ++) {
                    s.texture(0, 0);
                    s.texture(l / TILE_WIDTH, 0);
                    s.texture(l / TILE_WIDTH, h / TILE_WIDTH);
                    s.texture(l / TILE_WIDTH, h / TILE_WIDTH);
                    s.texture(0, h / TILE_WIDTH);
                    s.texture(0, 0);
                }
                
                for (int i = 0; i < 2; i ++) {
                    s.texture(0, 0);
                    s.texture(w / TILE_WIDTH, 0);
                    s.texture(w / TILE_WIDTH, l / TILE_WIDTH);
                    s.texture(w / TILE_WIDTH, l / TILE_WIDTH);
                    s.texture(0, l / TILE_WIDTH);
                    s.texture(0, 0);
                }
                
                for (int i = 0; i < 2; i ++) {
                    s.texture(0, 0);
                    s.texture(w / TILE_WIDTH, 0);
                    s.texture(w / TILE_WIDTH, h / TILE_WIDTH);
                    s.texture(w / TILE_WIDTH, h / TILE_WIDTH);
                    s.texture(0, h / TILE_WIDTH);
                    s.texture(0, 0);
                }
            }

            s.vertex(x, y + h, z);
            s.vertex(x, y + h, z + l);
            s.vertex(x, y, z + l);
            s.vertex(x, y, z + l);
            s.vertex(x, y, z);
            s.vertex(x, y + h, z);

            s.vertex(x + w, y + h, z + l);
            s.vertex(x + w, y + h, z);
            s.vertex(x + w, y, z);
            s.vertex(x + w, y, z);
            s.vertex(x + w, y, z + l);
            s.vertex(x + w, y + h, z + l);

            s.vertex(x + w, y, z);
            s.vertex(x, y, z);
            s.vertex(x, y, z + l);
            s.vertex(x, y, z + l);
            s.vertex(x + w, y, z + l);
            s.vertex(x + w, y, z);
            
            s.vertex(x, y + h, z);
            s.vertex(x + w, y + h, z);
            s.vertex(x + w, y + h, z + l);
            s.vertex(x + w, y + h, z + l);
            s.vertex(x, y + h, z + l);
            s.vertex(x, y + h, z);

            s.vertex(x + w, y + h, z);
            s.vertex(x, y + h, z);
            s.vertex(x, y, z);
            s.vertex(x, y, z);
            s.vertex(x + w, y, z);
            s.vertex(x + w, y + h, z);

            s.vertex(x, y + h, z + l);
            s.vertex(x + w, y + h, z + l);
            s.vertex(x + w, y, z + l);
            s.vertex(x + w, y, z + l);
            s.vertex(x, y, z + l);
            s.vertex(x, y + h, z + l);
            
            for (int i = 0; i < 36; i ++) s.color(_color);

            for (int i = 0; i < 6; i ++) s.sprite(_westsprite);
            for (int i = 0; i < 6; i ++) s.sprite(_eastsprite);
            for (int i = 0; i < 6; i ++) s.sprite(_bottomsprite);
            for (int i = 0; i < 6; i ++) s.sprite(_topsprite);
            for (int i = 0; i < 6; i ++) s.sprite(_northsprite);
            for (int i = 0; i < 6; i ++) s.sprite(_southsprite);

            if (_unlit) for (int i = 0; i < 36; i ++) s.normal(0, 0, 0);
            else {
                for (int i = 0; i < 6; i ++) s.normal(-1, 0, 0);
                for (int i = 0; i < 6; i ++) s.normal(1, 0, 0);
                for (int i = 0; i < 6; i ++) s.normal(0, -1, 0);
                for (int i = 0; i < 6; i ++) s.normal(0, 1, 0);
                for (int i = 0; i < 6; i ++) s.normal(0, 0, -1);
                for (int i = 0; i < 6; i ++) s.normal(0, 0, 1);
            }
        }

        void cone(Sketch& s, float x, float y, float z, float w, float h, float l, int steps) {
            vec4 uv = _uv;
            if (_autouv) {
                uv = { 0, 0, w / TILE_WIDTH, l / TILE_WIDTH };
            }
            float cx = x + w / 2, cz = z + l / 2, xr = w / 2, zr = l / 2;
            float cu = uv[0] + uv[2] / 2, cv = uv[1] + _uv[3] / 2, ur = uv[2] / 2, vr = uv[3] / 2;
            float step = (2 * PI) / steps;
            for (float i = 0; i < 2 * PI - 0.001; i += step) {
                float dx = xr * sin(i), dz = zr * cos(i);
                float du = ur * sin(i), dv = vr * cos(i);
                float nx = xr * sin(i + step), nz = zr * cos(i + step);
                float nu = ur * sin(i + step), nv = vr * cos(i + step);

                // bottom
                for (int i = 0; i < 3; i ++) {
                    s.normal(0, -1, 0); s.sprite(_bottomsprite); s.color(_color);
                }
                s.vertex(cx, y, cz); s.texture(cu, cv);
                s.vertex(cx + dx, y, cz + dz); s.texture(cu + du, cv + dv);
                s.vertex(cx + nx, y, cz + nz); s.texture(cu + nu, cv + nv);

                // top
                for (int i = 0; i < 3; i ++) {
                    s.sprite(_topsprite); s.color(_color);
                }
                s.normal(0, 1, 0); s.normal(nx, 0, nz); s.normal(dx, 0, dz); 
                s.vertex(cx, y + h, cz); s.texture(cu, cv);
                s.vertex(cx + nx, y, cz + nz); s.texture(cu + nu, cv + nv);
                s.vertex(cx + dx, y, cz + dz); s.texture(cu + du, cv + dv);
            }
        }

        void cyl(Sketch& s, float x, float y, float z, float w, float h, float l, int steps) {
            vec4 vertuv = _uv, sideuv = _uv;
            if (_autouv) {
                vertuv = { 0, 0, w / TILE_WIDTH, l / TILE_WIDTH };
                sideuv = { 0, 0, w / TILE_WIDTH, h / TILE_WIDTH };
            }
            float cx = x + w / 2, cz = z + l / 2, xr = w / 2, zr = l / 2;
            float cu = vertuv[0] + vertuv[2] / 2, cv = vertuv[1] + _uv[3] / 2, ur = vertuv[2] / 2, vr = vertuv[3] / 2;
            float step = (2 * PI) / steps;
            for (float i = 0; i < 2 * PI - 0.001; i += step) {
                float dx = xr * sin(i), dz = zr * cos(i);
                float du = ur * sin(i), dv = vr * cos(i);
                float nx = xr * sin(i + step), nz = zr * cos(i + step);
                float nu = ur * sin(i + step), nv = vr * cos(i + step);

                // bottom
                for (int i = 0; i < 3; i ++) {
                    s.normal(0, -1, 0); s.sprite(_bottomsprite); s.color(_color);
                }
                s.vertex(cx, y, cz); s.texture(cu, cv);
                s.vertex(cx + dx, y, cz + dz); s.texture(cu + du, cv + dv);
                s.vertex(cx + nx, y, cz + nz); s.texture(cu + nu, cv + nv);

                // top
                for (int i = 0; i < 3; i ++) {
                    s.normal(0, 1, 0); s.sprite(_topsprite); s.color(_color);
                }
                s.vertex(cx, y + h, cz); s.texture(cu, cv);
                s.vertex(cx + nx, y + h, cz + nz); s.texture(cu + nu, cv + nv);
                s.vertex(cx + dx, y + h, cz + dz); s.texture(cu + du, cv + dv);

                // side
                for (int i = 0; i < 6; i ++) {
                    s.sprite(_sidesprite); s.color(_color);
                }
                vec3 dnorm = { dx / xr, 0, dz / zr };
                vec3 nnorm = { nx / xr, 0, nz / zr };
                float wu = sideuv[0] + sideuv[2] * i / PI, wv = sideuv[1];
                float dwu = sideuv[2] * 2.0 / steps;
                s.vertex(cx + dx, y + h, cz + dz); s.texture(wu, wv); s.normal(dnorm);
                s.vertex(cx + nx, y + h, cz + nz); s.texture(wu + dwu, wv); s.normal(nnorm);
                s.vertex(cx + nx, y, cz + nz); s.texture(wu + dwu, wv + sideuv[3]); s.normal(nnorm);
                s.vertex(cx + nx, y, cz + nz); s.texture(wu + dwu, wv + sideuv[3]); s.normal(nnorm);
                s.vertex(cx + dx, y, cz + dz); s.texture(wu, wv + sideuv[3]); s.normal(dnorm);
                s.vertex(cx + dx, y + h, cz + dz); s.texture(wu, wv); s.normal(dnorm);
            }
        }

        void frustum(Sketch& s, float x, float y, float z, float w, float h, float l, float w2, float l2, int steps) {
            vec4 vertuv = _uv, sideuv = _uv, vertuv2 = _uv;
            if (_autouv) {
                vertuv = { 0, 0, w / TILE_WIDTH, l / TILE_WIDTH };
                sideuv = { 0, 0, w / TILE_WIDTH, h / TILE_WIDTH };
                vertuv2 = { 0, 0, w2 / TILE_WIDTH, l2 / TILE_WIDTH };
            }
            float cx = x + w / 2, cz = z + l / 2, xr = w / 2, zr = l / 2;
            float xr2 = w2 / 2, zr2 = l2 / 2;
            float cu = vertuv[0] + vertuv[2] / 2, cv = vertuv[1] + _uv[3] / 2, ur = vertuv[2] / 2, vr = vertuv[3] / 2;
            float ur2 = vertuv2[2] / 2, vr2 = vertuv2[3] / 2;
            float step = (2 * PI) / steps;
            for (float i = 0; i < 2 * PI - 0.001; i += step) {
                float dx = xr * sin(i), dz = zr * cos(i);
                float du = ur * sin(i), dv = vr * cos(i);
                float nx = xr * sin(i + step), nz = zr * cos(i + step);
                float nu = ur * sin(i + step), nv = vr * cos(i + step);

                float dx2 = xr2 * sin(i), dz2 = zr2 * cos(i);
                float du2 = ur2 * sin(i), dv2 = vr2 * cos(i);
                float nx2 = xr2 * sin(i + step), nz2 = zr2 * cos(i + step);
                float nu2 = ur2 * sin(i + step), nv2 = vr2 * cos(i + step);

                // bottom
                for (int i = 0; i < 3; i ++) {
                    s.normal(0, -1, 0); s.sprite(_bottomsprite); s.color(_color);
                }
                s.vertex(cx, y, cz); s.texture(cu, cv);
                s.vertex(cx + dx, y, cz + dz); s.texture(cu + du, cv + dv);
                s.vertex(cx + nx, y, cz + nz); s.texture(cu + nu, cv + nv);

                // top
                for (int i = 0; i < 3; i ++) {
                    s.normal(0, 1, 0); s.sprite(_topsprite); s.color(_color);
                }
                s.vertex(cx, y + h, cz); s.texture(cu, cv);
                s.vertex(cx + nx2, y + h, cz + nz2); s.texture(cu + nu2, cv + nv2);
                s.vertex(cx + dx2, y + h, cz + dz2); s.texture(cu + du2, cv + dv2);

                // side
                for (int i = 0; i < 6; i ++) {
                    s.sprite(_sidesprite); s.color(_color);
                }
                vec3 dnorm = { dx / xr, 0, dz / zr };
                vec3 nnorm = { nx / xr, 0, nz / zr };
                float wu = sideuv[0] + sideuv[2] * i / PI, wv = sideuv[1];
                float dwu = sideuv[2] * 2.0 / steps;
                s.vertex(cx + dx, y + h, cz + dz); s.texture(wu, wv); s.normal(dnorm);
                s.vertex(cx + nx, y + h, cz + nz); s.texture(wu + dwu, wv); s.normal(nnorm);
                s.vertex(cx + nx, y, cz + nz); s.texture(wu + dwu, wv + sideuv[3]); s.normal(nnorm);
                s.vertex(cx + nx, y, cz + nz); s.texture(wu + dwu, wv + sideuv[3]); s.normal(nnorm);
                s.vertex(cx + dx, y, cz + dz); s.texture(wu, wv + sideuv[3]); s.normal(dnorm);
                s.vertex(cx + dx, y + h, cz + dz); s.texture(wu, wv); s.normal(dnorm);
            }
        }

        void ball(Sketch& s, float x, float y, float z, float w, float h, float l, int xsteps, int ysteps) {
            vec4 uv = _uv;
            if (_autouv) {
                uv = { 0, 0, w / TILE_WIDTH, h / TILE_WIDTH };
            }
            float cx = x + w / 2, cy = y + h / 2, cz = z + l / 2, xr = w / 2, yr = h / 2, zr = l / 2;
            float xstep = (2 * PI) / xsteps, ystep = PI / ysteps;
            for (float i = 0; i < 2 * PI - 0.001; i += xstep) {
                for (float j = -PI / 2; j < PI / 2 - 0.001; j += ystep) {
                    float dx1 = xr * sin(i) * cos(j + ystep), dy1 = yr * sin(j + ystep), dz1 = zr * cos(i) * cos(j + ystep);
                    float dx2 = xr * sin(i + xstep) * cos(j + ystep), dy2 = yr * sin(j + ystep), dz2 = zr * cos(i + xstep) * cos(j + ystep);
                    float dx3 = xr * sin(i + xstep) * cos(j), dy3 = yr * sin(j), dz3 = zr * cos(i + xstep) * cos(j);
                    float dx4 = xr * sin(i) * cos(j), dy4 = yr * sin(j), dz4 = zr * cos(i) * cos(j);

                    vec3 n1 = { dx1 / xr, dy1 / yr, dz1 / zr };
                    vec3 n2 = { dx2 / xr, dy2 / yr, dz2 / zr };
                    vec3 n3 = { dx3 / xr, dy3 / yr, dz3 / zr };
                    vec3 n4 = { dx4 / xr, dy4 / yr, dz4 / zr };
                    float u = uv[0] + uv[2] * i / PI, v = uv[1] + uv[3] * (j + PI / 2) / PI;
                    float du = uv[2] * (2.0 / xsteps), dv = -uv[3] / ysteps;
                    s.vertex(cx + dx1, cy + dy1, cz + dz1); s.texture(u, v); s.normal(n1);
                    s.vertex(cx + dx2, cy + dy2, cz + dz2); s.texture(u + du, v); s.normal(n2);
                    s.vertex(cx + dx3, cy + dy3, cz + dz3); s.texture(u + du, v + dv); s.normal(n3);
                    s.vertex(cx + dx3, cy + dy3, cz + dz3); s.texture(u + du, v + dv); s.normal(n3);
                    s.vertex(cx + dx4, cy + dy4, cz + dz4); s.texture(u, v + dv); s.normal(n4);
                    s.vertex(cx + dx1, cy + dy1, cz + dz1); s.texture(u, v); s.normal(n1);

                    for (int i = 0; i < 6; i ++) s.sprite(_sprite), s.color(_color);
                }
            }
        }

        void text(Sketch& s, float x, float y, const char* str, float hsize, float vsize, float linewidth) {
            float column = 0, line = 0;
            while (*str) {
                const char* ptr = str;
                float width = column;
                while (*ptr && !isspace(*ptr)) {
                    width += hsize;
                    ptr ++;
                }
                if (width > linewidth) line += vsize, column = 0;
                while (*str && !isspace(*str)) {
                    float u = 16 * (*str % 16), v = 16 * (*str / 16);
                    s.vertex(x + column - hsize / 2, y + line - hsize / 2, 0);
                    s.vertex(x + column + 3 * hsize / 2, y + line - hsize / 2, 0);
                    s.vertex(x + column + 3 * hsize / 2, y + line + 3 * hsize / 2, 0);
                    s.vertex(x + column + 3 * hsize / 2, y + line + 3 * hsize / 2, 0);
                    s.vertex(x + column - hsize / 2, y + line + 3 * hsize / 2, 0);
                    s.vertex(x + column - hsize / 2, y + line - hsize / 2, 0);
                    for (int i = 0; i < 6; i ++) {
                        s.normal(0, 0, -1), s.color(_color); 
                        s.sprite(u, v, 16, 16);
                    }
                    s.texture(0, 0);
                    s.texture(1, 0);
                    s.texture(1, 1);
                    s.texture(1, 1);
                    s.texture(0, 1);
                    s.texture(0, 0);
                    column += hsize;
                    str ++;
                }
                while (isspace(*str)) {
                    if (*str == '\n') line += vsize, column = 0;
                    else column += hsize;
                    str ++;
                }
            }
        }
    }

    namespace transform {
        vector<mat4> _state;
        mat4 _tr = art::identity();
        
        void push() {
            _state.push_back(_tr);
        }

        void pop() {
            if (_state.size()) {
                _tr = _state.back();
                _state.pop_back();
            }
            else _tr = art::identity();
        }

        void xrot(float degrees) {
            _tr = _tr * xrotate(radians(degrees));
            activeShader().with("transform", _tr);
        }

        void yrot(float degrees) {
            _tr = _tr * yrotate(radians(degrees));
            activeShader().with("transform", _tr);
        }

        void zrot(float degrees) {
            _tr = _tr * zrotate(radians(degrees));
            activeShader().with("transform", _tr);
        }

        void shift(float x, float y, float z) {
            _tr = _tr * translate(x, y, z);
            activeShader().with("transform", _tr);
        }

        void scale(float sx, float sy, float sz) {
            _tr = _tr * art::scale(sx, sy, sz);
            activeShader().with("transform", _tr);
        }

        void scale(float s) {
            _tr = _tr * art::scale(s);
            activeShader().with("transform", _tr);
        }

        void identity() {
            _tr = art::identity();
            activeShader().with("transform", _tr);
        }
    }

    namespace view {
        vec3 _pos;
        float _yaw, _pitch;
        vec3 _look, _right, _up;

        void setCamera(vec3 pos, float yaw, float pitch) {
            _pos = pos;
            _yaw = radians(yaw);
            _pitch = radians(pitch);

            _look = {
                sin(-_yaw) * cos(_pitch),
                sin(_pitch),
                cos(-_yaw) * cos(_pitch)
            };

            _right = vec3{
                cos(-_yaw),
                0,
                -sin(-_yaw)
            };

            _up = cross(_look, _right);
        }

        vec3 look() {
            return _look;
        }

        vec3 right() {
            return _right;
        }

        vec3 up() {
            return _up;
        }

        vec3 pos() {
            return _pos;
        }

        mat4 translation() {
            return scale(3.0) * translate(-_pos[0], -_pos[1], -_pos[2]);
        }

        mat4 matrix() {
            return scale(3.0) * xrotate(_pitch) * yrotate(_yaw) * translate(-_pos[0], -_pos[1], -_pos[2]);
        }
    }

    namespace draw {
        void board(Sketch& s, float x, float y, float z, float w, float h) {
            vec3 center = { x, y, z };
            vec3 l = view::right() * -0.5 * w, r = view::right() * 0.5 * w;
            vec3 u = view::up() * 0.5 * h, d = view::up() * -0.5 * h;

            for (int i = 0; i < 6; i ++) {
                s.color(_color);
                s.normal(view::look() * -1);
                s.sprite(_sprite);
            }

            if (_autouv) {
                s.texture(0, h / TILE_WIDTH);  
                s.texture(w / TILE_WIDTH, h / TILE_WIDTH); 
                s.texture(w / TILE_WIDTH, 0);
                s.texture(w / TILE_WIDTH, 0);
                s.texture(0, 0);
                s.texture(0, h / TILE_WIDTH);
            }
            else {
                s.texture(_uv[0], _uv[1] + _uv[3]);
                s.texture(_uv[0] + _uv[2], _uv[1] + _uv[3]);
                s.texture(_uv[0] + _uv[2], _uv[1]);
                s.texture(_uv[0] + _uv[2], _uv[1]);
                s.texture(_uv[0], _uv[1]);
                s.texture(_uv[0], _uv[1] + _uv[3]);
            }

            s.vertex(center + l + d); 
            s.vertex(center + r + d); 
            s.vertex(center + r + u); 
            s.vertex(center + r + u); 
            s.vertex(center + l + u); 
            s.vertex(center + l + d); 
        }
    }

    namespace light {
        vec3 _direction;
        float _intensity;

        struct Light {
            vec3 pos;
            vec4 color;
            float radius;
        };

        class PollEvent {
            vector<Light> lights;
            GLuint i = 0;
        public:
            void addLight(Light l) {
                if ((l.pos - view::pos()).dist() < 720) {
                    lights.push_back(l);
                    i ++;
                }
            }

            void bind() {
                sort(lights.begin(), lights.end(), [&](const Light& a, const Light& b) -> bool {
                    return (b.pos - view::pos()).distsq() < (a.pos - view::pos()).distsq();
                });
                for (int j = 0; j < (i < 32 ? i : 32); j ++) {
                    string pos = "lights[";
                    pos += to_string(j);
                    pos += "]";
                    string col = pos + ".col";
                    string rad = pos + ".radius";
                    pos += ".pos";
                    activeShader().with(pos.c_str(), lights[j].pos);
                    activeShader().with(col.c_str(), lights[j].color);
                    activeShader().with(rad.c_str(), lights[j].radius);
                }
                for (int j = i; j < 32; j ++) {
                    string pos = "lights[";
                    pos += to_string(j);
                    pos += "]";
                    string col = pos + ".col";
                    string rad = pos + ".radius";
                    pos += ".pos";
                    activeShader().with(pos.c_str(), vec3(0, 0, 0));
                    activeShader().with(col.c_str(), vec4(0, 0, 0, 0));
                    activeShader().with(rad.c_str(), 0);
                }
            }
        };

        float intensity() {
            return _intensity;
        }

        void setIntensity(float i) {
            _intensity = i;
        }

        vec3 direction() {
            return _direction;
        }

        void setDirection(vec3 v) {
            _direction = v.normalize();
            _direction[1] *= -1;
        }

        mat4 view() {
            float yaw = atan2(_direction[0], _direction[2]);
            float pitch = atan2(-_direction[1], _direction.dist());
            return xrotate(pitch) * yrotate(yaw);
        }
    }

    ////////////////
    //            //
    //  Objects!  //
    //            //
    ////////////////

    class Drawable {
    public:
        virtual void draw(float dt) = 0;
    };

    class Tickable {
    public:
        virtual void update(float dt) = 0;
    };

    class Object;

    class Collider {
    public:
        virtual bool intersects(Object* o) const = 0;
        virtual vec3 expel(Object* o) const = 0;
    };

    struct box {
        float x, y, z, w, h, l;

        bool intersects(vec3 point) const {
            return point[0] >= x && point[1] >= y && point[2] >= z
                && point[0] <= x + w && point[1] <= y + h && point[2] <= z + l;
        }

        bool intersects(box other) const {
            return !(
                other.x > x + w || other.y > y + h || other.z > z + l ||
                other.x + other.w < x || other.y + other.h < y || other.z + other.l < z
            ); 
        }

        box offset(vec3 pos) const {
            return { x + pos[0], y + pos[1], z + pos[2], w, h, l };
        }

        box expand(float amount) const {
            return { x - amount, y - amount, z - amount, w + amount * 2, h + amount * 2, l + amount * 2 };
        }

        vec3 center() const {
            return { x + w / 2, y + h / 2, z + l / 2 };
        }
    };

    class ObjectLayer;

    class Object {
        int _id;
        box _bounds;
        Texture* _batch;
        ObjectLayer* _layer;
        bool _static, _alive, _solid, _active = true;
        float lifetime;
    protected:
        vec3 pos, vel, esc;
        void setBatch(Texture* tex) {
            _batch = tex;
        }

        void setId(int id) {
            _id = id;
        }

        void setLayer(ObjectLayer* layer) {
            _layer = layer;
        }

        void setBounds(const box& b) {
            _bounds = b;
        }

        void makeStatic() {
            _static = true;
        }

        void noclip() {
            _solid = false;
        }

        friend class ObjectLayer;
    public:
        Object(vec3 position): _id(-1),
            _bounds{0, 0, 0, 16, 16, 16}, pos(position),
            _batch(nullptr), _static(false), _alive(true),
            _solid(true), lifetime(0) {}
        virtual ~Object() {}
        Object(const Object& other) = delete;
        Object& operator=(const Object& other) = delete;

        void deactivate() {
            _active = false;
        }

        void activate() {
            _active = true;
        }

        bool active() const {
            return _active;
        }

        Texture* batch() const {
            return _batch;
        }

        int id() const {
            return _id;
        }

        bool alive() const {
            return _alive;
        }

        void kill() {
            _alive = false;
        }

        box bounds() const {
            return _bounds.offset(pos);
        }

        virtual void update(float dt) {
            lifetime += dt;
            pos += vel * dt;
        }

        void collide(vector<Collider*>& colliders, float dt) {
            esc[0] = 0;
            esc[1] = 0;
            esc[2] = 0;
            if (_solid) {
                for (Collider* c : colliders) {
                    if (c->intersects(this)) {
                        vec3 escape = c->expel(this);
                        pos += escape;
                        esc += escape;
                        if (escape[0] != 0 && vel[0] / escape[0] < 0) vel[0] = 0;
                        if (escape[2] != 0 && vel[2] / escape[2] < 0) vel[2] = 0;
                        if (escape[1] != 0 && vel[1] / escape[1] < 0) vel[1] = 0;
                    }
                }
            }
        }

        ObjectLayer* layer() {
            return _layer;
        }

        const ObjectLayer* layer() const {
            return _layer;
        }

        virtual void draw(float dt) {
            //
        }

        virtual void draw(Sketch& buf, float dt) {
            //
        }

        float age() const {
            return lifetime;
        }

        bool dynamic() const {
            return !_static;
        }

        virtual void addColliders(vector<Collider*>& colliders, float dt) {
            //
        }
    };

    struct ChunkEntry {
        int x, y, z;
        bool operator==(const ChunkEntry& c) const {
            return c.x == x && c.y == y && c.z == z;
        }
        bool operator!=(const ChunkEntry& c) const {
            return !operator==(c);
        }
    };
}

namespace std {
    template<>
    struct hash<art::ChunkEntry> {
        size_t operator()(const art::ChunkEntry& entry) const {
            return hash<int>{}(entry.x) 
                ^ hash<int>{}(entry.y)
                ^ hash<int>{}(entry.z);
        }
    };
}

namespace art {
    #define SPACE_GRANULARITY 32

    class ObjectLayer : public Drawable, public Tickable {
        ObjectLayer* parent = nullptr;
        vector<ObjectLayer*> children;
        unordered_set<Object*> objects;
        unordered_set<Object*> dynamic;

        unordered_map<Texture*, Sketch> staticBatches;
        unordered_map<Texture*, Sketch> dynamicBatches;
        unordered_map<ChunkEntry, unordered_set<Object*>> space;
        float _timescale = 1;
        bool _paused = false;
        bool doesCollision;
    public:
        ObjectLayer(bool doCollision = false): doesCollision(doCollision) {}
        virtual ~ObjectLayer() {
            for (ObjectLayer* l : children) delete l;
            for (Object* o : objects) delete o;
        }

        ObjectLayer(const ObjectLayer& other) = delete;
        ObjectLayer& operator=(const ObjectLayer& other) = delete;

        void addChild(ObjectLayer* layer) {
            children.push_back(layer);
            layer->parent = this;
        }

        void clear() {
            for (Object* o : objects) delete o;
            objects.clear();
            dynamic.clear();
            space.clear();
            staticBatches.clear();
            dynamicBatches.clear();
            for (ObjectLayer* l : children) l->clear();
        }

        void pause() {
            _paused = true;
        }

        void unpause() {
            _paused = false;
        }

        float setTimescale(float scale) {
            _timescale = scale;
        }

        float timescale() const {
            return _timescale;
        }

        Object* add(Object* object) {
            object->setId(objects.size());
            object->setLayer(this);
            objects.insert(object);
            if (object->dynamic()) dynamic.insert(object);
            else {
                object->draw(staticBatches[object->batch()], 0);
            
                box b = object->bounds();
                for (float x = b.x; x < b.x + b.w; x += SPACE_GRANULARITY) {
                    for (float y = b.y; y < b.y + b.h; y += SPACE_GRANULARITY) {
                        for (float z = b.z; z < b.z + b.l; z += SPACE_GRANULARITY) {
                            ChunkEntry e = {
                                (int)floor(x / SPACE_GRANULARITY),
                                (int)floor(y / SPACE_GRANULARITY),
                                (int)floor(z / SPACE_GRANULARITY)
                            };
                            space[e].insert(object);
                        }
                    }
                }
            }
            return object;
        }

        void remove(Object* object) {
            box b = object->bounds();
            for (float x = b.x; x < b.x + b.w; x += SPACE_GRANULARITY) {
                for (float y = b.y; y < b.y + b.h; y += SPACE_GRANULARITY) {
                    for (float z = b.z; z < b.z + b.l; z += SPACE_GRANULARITY) {
                        ChunkEntry e = {
                            (int)floor(x / SPACE_GRANULARITY),
                            (int)floor(y / SPACE_GRANULARITY),
                            (int)floor(z / SPACE_GRANULARITY)
                        };
                        space[e].erase(object);
                    }
                }
            }
            if (object->dynamic()) dynamic.erase(object);
            objects.erase(object);
            delete object;
        }

        unordered_set<Object*> objectsNear(vec3 point) {
            unordered_set<Object*> objects;
            ChunkEntry e = {
                (int)floor(point[0] / SPACE_GRANULARITY),
                (int)floor(point[1] / SPACE_GRANULARITY),
                (int)floor(point[2] / SPACE_GRANULARITY)
            };
            for (Object* o : space[e]) objects.insert(o);
            for (Object* o : dynamic) if (o->bounds().intersects(point)) objects.insert(o);
            for (ObjectLayer* l : children) {
                for (Object* o : l->objectsNear(point)) objects.insert(o);
            }
            return objects;
        }

        unordered_set<Object*> objectsNear(box b) {
            unordered_set<Object*> objects;
            for (float x = b.x; x < b.x + b.w; x += SPACE_GRANULARITY) {
                for (float y = b.y; y < b.y + b.h; y += SPACE_GRANULARITY) {
                    for (float z = b.z; z < b.z + b.l; z += SPACE_GRANULARITY) {
                        ChunkEntry e = {
                            (int)floor(x / SPACE_GRANULARITY),
                            (int)floor(y / SPACE_GRANULARITY),
                            (int)floor(z / SPACE_GRANULARITY)
                        };
                        for (Object* o : space[e]) objects.insert(o);
                    }
                }
            }
            for (Object* o : dynamic) if (o->bounds().intersects(b)) objects.insert(o);
            for (ObjectLayer* l : children) {
                for (Object* o : l->objectsNear(b)) objects.insert(o);
            }
            return objects;
        }

        unordered_set<Object*> dynamicIn(box b) {
            unordered_set<Object*> objects;
            for (Object* o : dynamic) if (o->bounds().intersects(b)) objects.insert(o);
            for (ObjectLayer* l : children) {
                for (Object* o : l->dynamicIn(b)) objects.insert(o);
            }
            return objects;
        }

        void update(float dt) override {
            float scaledDt = _paused ? 0 : dt * _timescale;

            vector<Object*> toClean;
            for (auto o : dynamic) {
                if (!o->alive()) {
                    toClean.push_back(o);
                }
            }
            for (auto o : toClean) {
                dynamic.erase(o);
                objects.erase(o);
                delete o;
            }
                
            for (auto o : dynamic) {
                 if (!_paused) o->update(scaledDt);

                vector<Collider*> cs;
                if (o->_solid) {
                    ObjectLayer* l = this;
                    while (l && !l->doesCollision) l = l->parent;
                    for (Object* b : l->objectsNear(o->bounds().expand(SPACE_GRANULARITY))) if (b->_solid) b->addColliders(cs, scaledDt);
                    for (Object* b : dynamic) if (b->_solid && b != o) b->addColliders(cs, scaledDt);
                    o->collide(cs, scaledDt);
                }
            }
            for (ObjectLayer* l : children) if (!_paused) l->update(scaledDt);
        }

        void draw(float dt) override {
            float scaledDt = _paused ? 0 : dt * _timescale;
            for (auto& p : staticBatches) {
                transform::identity();
                p.first->bind();

                if (p.first->additive()) glBlendFunc(GL_SRC_ALPHA, GL_ONE);
                else glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
                if (p.first->translucent()) glDepthMask(false);
                else glDepthMask(true);

                p.second.draw();
                p.first->unbind();
            }
            glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
            glDepthMask(true);

            for (auto& p : dynamicBatches) {
                p.second.clear();
            }
            for (auto& o : dynamic) {
                auto tex = o->batch();
                
                if (tex) o->draw(dynamicBatches[tex], scaledDt);
                else o->draw(scaledDt);
            }

            for (auto& p : dynamicBatches) {
                transform::identity();
                p.first->bind();
                
                if (p.first->additive()) glBlendFunc(GL_SRC_ALPHA, GL_ONE);
                else glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
                if (p.first->translucent()) glDepthMask(false);
                else glDepthMask(true);

                p.second.draw();
                p.first->unbind();
            }
            glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
            glDepthMask(true);

            for (ObjectLayer* l : children) l->draw(scaledDt);
        }
    };

    class BoxCollider : public Collider {
        box b;
        vec3 norm;
    public:
        BoxCollider(const box& bounds): b(bounds) {
            float m = min(min(b.w, b.h), b.l);
            norm = { b.w / m, b.h / m, b.l / m };
        }

        virtual bool intersects(Object* o) const override {
            return b.intersects(o->bounds());
        }

        virtual vec3 expel(Object* o) const override {
            const box& a = o->bounds();
		    vec3 ac = { a.x + a.w / 2, a.y + a.h / 2, a.z + a.l / 2 };
            vec3 bc = { b.x + b.w / 2, b.y + b.h / 2, b.z + b.l / 2 };
            vec3 diff = (ac - bc);
            float dx = diff[0] < 0 ? b.x - (a.x + a.w) : (b.x + b.w) - a.x;
            float dy = diff[1] < 0 ? b.y - (a.y + a.h) : (b.y + b.h) - a.y;
            float dz = diff[2] < 0 ? b.z - (a.z + a.l) : (b.z + b.l) - a.z;
            if (abs(dx) < abs(dy) && abs(dx) < abs(dz)) {
                return { dx, 0, 0 };
            }
            else if (abs(dz) < abs(dy)) {
                return { 0, 0, dz };
            }
            else {
                return { 0, dy, 0 };
            }
        }
    };

    class World : public Drawable {
        bool dirty = true;
        Sketch s1, s2;
        Texture t = Texture("dirt.png");
        Texture u = Texture("gem.png");
    public:
        void draw(float dt) override {
            Shader& shader = activeShader();
            shader.with("transform", scale(1.0f));

            t.bind();

            if (dirty) {
                draw::cyl(s1, -160, -64, -160, 320, 32, 320, 80);
                draw::ball(s1, -96, -128, -96, 192, 192, 192, 40, 40);
                draw::ball(s1, -128, 0, -128, 64, 64, 64, 40, 40);
                draw::ball(s1, -128, 0, 64, 64, 64, 64, 40, 40);
                draw::ball(s1, 64, 0, 64, 64, 64, 64, 40, 40);
                draw::ball(s1, 64, 0, -128, 64, 64, 64, 40, 40);
                draw::uv();
                dirty = false;
            }
            s1.draw();
            u.bind();
            s2.draw();
            
            u.unbind();
        }
    };

    //////////////////////////////////////////////////////////
    //                                                      //
    //  Canvases!                                           //
    //  --------------------------------------------------  //
    //  Canvases are layers of a scene. They are rendered   //
    //  within a Window. Each canvas is self-contained, so  //
    //  lots of things can be drawn to it piece by piece    //
    //  before it is actually displayed on the screen.      //
    //                                                      //
    //////////////////////////////////////////////////////////

    class Canvas {
    protected:
        int w, h;
        GLuint fbo, rbo, tex;
        Drawable* drawable;

        void free() {
            glDeleteTextures(1, &tex);
            glDeleteFramebuffers(1, &fbo);
            glDeleteRenderbuffers(1, &rbo);
        }

        void copy(const Canvas& other) {
            // TODO: Copy framebuffer data over.
        }

        void init(GLenum format = GL_RGBA, GLenum internalFormat = GL_RGBA, GLenum type = GL_UNSIGNED_BYTE) {
            glGenTextures(1, &tex);
            glGenFramebuffers(1, &fbo);
            glGenRenderbuffers(1, &rbo);

            glBindFramebuffer(GL_FRAMEBUFFER, fbo);
            glBindRenderbuffer(GL_RENDERBUFFER, rbo);
            glBindTexture(GL_TEXTURE_2D, tex);
            
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
            glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, w, h, 0, format,
                         type, nullptr);

            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                                   GL_TEXTURE_2D, tex, 0);
            
            glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT32, 
                                  w, h);
            glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                                      GL_RENDERBUFFER, rbo);

            glBindFramebuffer(GL_FRAMEBUFFER, 0);
            glBindRenderbuffer(GL_RENDERBUFFER, 0);
            glBindTexture(GL_TEXTURE_2D, 0);
        }

        void bindFBO() {
            glBindFramebuffer(GL_FRAMEBUFFER, fbo);
            glViewport(0, 0, w, h);
            glClearColor(0, 0, 0, 1);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        }

        void unbindFBO() {
            glBindFramebuffer(GL_FRAMEBUFFER, 0);
        }

        void toScreen(int width, int height) {
            glDisable(GL_DEPTH_TEST);
            glBindFramebuffer(GL_FRAMEBUFFER, 0);
            glViewport(0, 0, width, height);
            glBindTexture(GL_TEXTURE_2D, tex);
            passthrough::shader.bind();
            Sketch s;
            draw::rect(s, -1, -1, 2, 2);
            s.draw();
            glBindTexture(GL_TEXTURE_2D, 0);
            glEnable(GL_DEPTH_TEST);
        }

    public:
        Canvas(int width, int height, 
               GLenum format = GL_RGBA, 
               GLenum internalFormat = GL_RGBA, 
               GLenum type = GL_UNSIGNED_BYTE):
            w(width), h(height) {
            init(format, internalFormat, type);
        }

        ~Canvas() {
            free();
        }

        Canvas(const Canvas& other): Canvas(other.w, other.h) {
            copy(other);
        }

        Canvas& operator=(const Canvas& other) {
            if (this != &other) {
                free();
                init();
                copy(other);
            }
            return *this;
        }

        int width() const {
            return w;
        }

        int height() const {
            return h;
        }

        void setDrawable(Drawable* d) {
            drawable = d;
        }

        void bind() {
            glBindTexture(GL_TEXTURE_2D, tex);
        }

        void unbind() {
            glBindTexture(GL_TEXTURE_2D, 0);
        }

        virtual void draw(int width, int height, float dt) {
            bindFBO();
            drawable->draw(dt);
            toScreen(width, height);
        }
    };

    class ShadowTestPass : public Canvas {
    public:
        ShadowTestPass(int width, int height): Canvas(width, height, GL_RED, GL_R32F, GL_FLOAT) {
            //
        }

        void draw(int width, int height, float dt) override {
            bindFBO();
            glClearColor(1, 0, 0, 1);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            depth::shader.bind()
                .with("projection", ortho(0, w, h, 0, -2000, 2000))
                .with("view", translate(w / 2, h / 2, 0) * light::view() * view::translation())
                .with("transform", identity());
            // glCullFace(GL_FRONT);
            drawable->draw(dt);
            glCullFace(GL_BACK);
            unbindFBO();
            // toScreen(width, height);
        }
    };

    class LightPass : public Canvas {
    public:
        LightPass(int width, int height): Canvas(width, height) {
            //
        }

        void draw(int width, int height, float dt) override {
            bindFBO();
            light::shader.bind()
                .with("projection", ortho(0, w, h, 0, -1000, 1000))
                .with("view", translate(w / 2, h / 2, 0) * view::matrix())
                .with("transform", identity());
            light::PollEvent e;
            dispatch(e);
            e.bind();
            drawable->draw(dt);
            unbindFBO();
            // toScreen(width, height);
        }
    };

    class ShadowApplyPass : public Canvas {
        Canvas* test;
    public:
        ShadowApplyPass(Canvas* testpass, int width, int height): 
            Canvas(width, height), test(testpass) {
            //
        }

        void draw(int width, int height, float dt) override {
            bindFBO();
            glActiveTexture(GL_TEXTURE1);
            test->bind();
            glActiveTexture(GL_TEXTURE0);
            shadow::shader.bind()
                .with("o_projection", ortho(0, w, h, 0, -2000, 2000))
                .with("o_view", translate(w / 2, h / 2, 0) * light::view() * view::translation())
                .with("o_transform", identity())
                .with("projection", ortho(0, w, h, 0, -2000, 2000))
                .with("view", translate(w / 2, h / 2, 0) * view::matrix())
                .with("transform", identity())
                .with("light", light::direction())
                .with("tex", 0)
                .with("depthbuf", 1)
                .with("width", w).with("height", h);
            drawable->draw(dt);
            glActiveTexture(GL_TEXTURE1);
            test->unbind();
            glActiveTexture(GL_TEXTURE0);
            unbindFBO();
            // toScreen(width, height);
        }
    };
    
    class WorldCanvas : public Canvas {
        Canvas *shadowpass, *lightpass;
    public:
        WorldCanvas(Canvas* shadows, Canvas* lights, int width, int height): 
            Canvas(width, height), shadowpass(shadows), lightpass(lights) {
            //
        }

        void draw(int width, int height, float dt) override {
            bindFBO();
            glActiveTexture(GL_TEXTURE1);
            shadowpass->bind();
            glActiveTexture(GL_TEXTURE2);
            lightpass->bind();
            glActiveTexture(GL_TEXTURE0);
            normal::shader.bind()
                .with("projection", ortho(0, w, h, 0, -1000, 1000))
                .with("view", translate(w / 2, h / 2, 0) * view::matrix())
                .with("transform", identity())
                .with("tex", 0)
                .with("shadows", 1)
                .with("lights", 2)
                .with("light", light::direction())
                .with("light_intensity", light::intensity())
                .with("width", w).with("height", h);
            drawable->draw(dt);
            glActiveTexture(GL_TEXTURE1);
            shadowpass->unbind();
            glActiveTexture(GL_TEXTURE2);
            lightpass->unbind();
            glActiveTexture(GL_TEXTURE0);
            unbindFBO();
            toScreen(width, height);
        }
    };
    
    class GuiCanvas : public Canvas {
    public:
        GuiCanvas(int width, int height): 
            Canvas(width, height) {
            //
        }

        void draw(int width, int height, float dt) override {
            bindFBO();
            glClearColor(0, 0, 0, 0);
            glClear(GL_COLOR_BUFFER_BIT);
            glDisable(GL_CULL_FACE);
            unlit::shader.bind()
                .with("projection", ortho(0, w, 0, h, -1000, 1000))
                .with("view", identity())
                .with("transform", identity())
                .with("tex", 0)
                .with("width", w).with("height", h);
            drawable->draw(dt);
            glEnable(GL_CULL_FACE);
            unbindFBO();
            toScreen(width, height);
        }
    };

    ///////////////////////
    //                   //
    //  Input Handling!  //
    //                   //
    ///////////////////////

    class Window;

    namespace input {
        GLFWwindow* window = nullptr;
        double _mx, _my;
        vec2 _mvec;

        void update(Window* window);

        bool mousedown(int code) {
            return glfwGetMouseButton(window, code);
        }

        bool keydown(int code) {
            return glfwGetKey(window, code) == GLFW_PRESS;
        }

        double mouseX() {
            return _mx;
        }

        double mouseY() {
            return _my;
        }

        vec2 mvec() {
            return _mvec;
        }
    }

    ////////////////////////////////////////////////////////
    //                                                    //
    //  Windows!                                          //
    //  ------------------------------------------------  //
    //  A Window is the highest level of rendering. Only  //
    //  one Window can be owned per program, since it     //
    //  handles loading the OpenGL context. Windows       //
    //  contain Canvas layers, and draw each layer in     //
    //  order every frame.                                //
    //                                                    //
    ////////////////////////////////////////////////////////

    class Window;

    unordered_map<GLFWwindow*, Window*> windows;

    class Window {
        GLFWwindow* win;
        int w, h;
        vector<Canvas*> layers;
        vector<Tickable*> tickables;
        double time, dt = 0;
    public:
        Window(int width, int height):
            w(width), h(height) {
            glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
            glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
            glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);
            glfwWindowHint(GLFW_VISIBLE, GLFW_TRUE);
            win = glfwCreateWindow(w, h, "Window", nullptr, nullptr);

            glfwMakeContextCurrent(win);
            gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);
            glfwSwapInterval(1);
            initGL();
            input::window = win;

            windows.insert({ win, this });
            glfwSetWindowSizeCallback(win, windowSizeCallback);
        }

        ~Window() {
            for (Canvas* layer : layers) delete layer;
            windows.erase(win);
            input::window = nullptr;
        }

        int add(Canvas* canvas) {
            layers.push_back(canvas);
            return layers.size() - 1;
        }

        Canvas* layer(int i) {
            return layers[i];
        }

        int width() const {
            return w;
        }

        int height() const {
            return h;
        }

        void close() {
            glfwSetWindowShouldClose(win, GLFW_TRUE);
        }

        bool closed() {
            return glfwWindowShouldClose(win);
        }

        void addTickable(Tickable* t) {
            tickables.push_back(t);
        }

        void update() {
            input::update(this);
            for (auto tickable : tickables) {
                tickable->update(dt);
            }
        }

        void draw() {
            glViewport(0, 0, w, h);
            glClearColor(0, 0, 0, 1);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

            for (Canvas* layer : layers) layer->draw(w, h, dt);   

            glfwSwapBuffers(win);
        }

        void run() {
            while (!glfwWindowShouldClose(win)) {
                time = glfwGetTime();
                update();
                draw();
                glfwPollEvents();
                double diff = glfwGetTime() - time;
                if (diff > 0.25) diff = 0.25;
                dt = dt * 0.9 + diff * 0.1;
            }
        }

        void resize(int width, int height) {
            w = width;
            h = height;
        }
    };

    void windowSizeCallback(GLFWwindow* window, int width, int height) {
        windows[window]->resize(width, height);
    }

    namespace input {
        void update(Window* w) {
            glfwGetCursorPos(window, &_mx, &_my);

            _mvec = vec2(_mx - w->width() / 2, _my - w->height() / 2).normalize();
        }
    }
}