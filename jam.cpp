#include "art.cpp"

using namespace art;

namespace rng {
    int i() {
        return rand();
    }

    int i(int n) {
        return rand() % n;
    }
    
    int i(int l, int n) {
        return l + (rand() % (n - l));
    }

    float f() {
        return rand() / (float)RAND_MAX;
    }
    
    float f(float max) {
        return max * f();
    }

    float f(float l, float h) {
        return l + (h - l) * f();
    }
}

Texture *TILE, *ENTITY, *GLOW, *WHITE, *MAP, *FONT, *GUI_TEXTURE;

void initTextures() {
    TILE = new Texture("asset/tile.png");
    ENTITY = new Texture("asset/entity.png");

    GLOW = new Texture("asset/glow.png");
    GLOW->setTranslucent(true);
    GLOW->setAdditive(true);

    WHITE = new Texture(1, 1);

    MAP = new Texture("asset/map.png");

    FONT = new Texture("asset/font.png");

    GUI_TEXTURE = new Texture("asset/gui.png");
}

void freeTextures() {
    delete TILE;
    delete ENTITY;
    delete GLOW;
    delete WHITE;
    delete MAP;
    delete FONT;
}

Music *CURRENT_MUSIC;

Music *MUSIC_L1, *FOOTSTEPS, *MUSIC_L2, *MUSIC_L3, *MUSIC_L4;
Sound *SOUND_PLAYER_SHOOT, *SOUND_ENEMY_HURT, *SOUND_ITEM_COLLECT,
      *SOUND_PLAYER_DEATH, *SOUND_BOTDOWN, *SOUND_BOTUP, *SOUND_INVULN;

void initAudio() {
    MUSIC_L1 = new Music("asset/level1.wav");
    MUSIC_L2 = new Music("asset/level2.wav");
    MUSIC_L3 = new Music("asset/level3.wav");
    MUSIC_L4 = new Music("asset/level4.wav");
    FOOTSTEPS = new Music("asset/sfx_footstep.wav");
    SOUND_PLAYER_SHOOT = new Sound("asset/sfx_player_laser.wav");
    SOUND_ENEMY_HURT = new Sound("asset/sfx_enemy_hurt.wav");
    SOUND_ITEM_COLLECT = new Sound("asset/sfx_item_collect.wav");
    SOUND_PLAYER_DEATH = new Sound("asset/sfx_death.wav");
    SOUND_BOTDOWN = new Sound("asset/sfx_botdown.wav");
    SOUND_BOTUP = new Sound("asset/sfx_botup.wav");
    SOUND_INVULN = new Sound("asset/sfx_invuln.wav");
}

void freeAudio() {
    delete MUSIC_L1;
    delete SOUND_PLAYER_SHOOT;
    delete SOUND_ENEMY_HURT;
    delete SOUND_ITEM_COLLECT;
    delete SOUND_PLAYER_DEATH;
    delete SOUND_BOTDOWN;
    delete SOUND_BOTUP;
    delete SOUND_INVULN;
}

class ObjectPlayer;
class ObjectRobot;

ObjectLayer *WORLD, *TERRAIN, *GUI, *CHARACTER, *ENTITIES, *ENEMIES;
ObjectPlayer *PLAYER;
ObjectRobot *ROBOT;

void initLayers() {
    WORLD = new ObjectLayer(true);
    GUI = new ObjectLayer();
    ENTITIES = new ObjectLayer();
    TERRAIN = new ObjectLayer();
    CHARACTER = new ObjectLayer();
    ENEMIES = new ObjectLayer();

    WORLD->addChild(ENTITIES);
    WORLD->addChild(TERRAIN);
    ENTITIES->addChild(ENEMIES);
    ENTITIES->addChild(CHARACTER);
}

void freeLayers() {
    delete WORLD;
}

int DEPTH_CANVAS, SHADOW_CANVAS, LIGHT_CANVAS, WORLD_CANVAS, GUI_CANVAS;

void initPasses(Window& win) {
    DEPTH_CANVAS = win.add(new ShadowTestPass(2880, 1920));
    SHADOW_CANVAS = win.add(new ShadowApplyPass(win.layer(DEPTH_CANVAS), 2880, 1920));
    LIGHT_CANVAS = win.add(new LightPass(1440, 960));
    WORLD_CANVAS = win.add(new WorldCanvas(win.layer(SHADOW_CANVAS), win.layer(LIGHT_CANVAS), 1440, 960));
    GUI_CANVAS = win.add(new GuiCanvas(1440, 960));
    win.layer(DEPTH_CANVAS)->setDrawable(WORLD);
    win.layer(SHADOW_CANVAS)->setDrawable(WORLD);
    win.layer(LIGHT_CANVAS)->setDrawable(WORLD);
    win.layer(WORLD_CANVAS)->setDrawable(WORLD);
    win.layer(GUI_CANVAS)->setDrawable(GUI);
}

class ObjectBox : public Object {
    BoxCollider coll;
    Texture* _tex;
    vec4 _sprite;
public:
    ObjectBox(box b, Texture* tex, vec4 sprite): 
        Object({ b.x, b.y, b.z }), coll(b), _tex(tex), _sprite(sprite) {
        setBounds({ 0, 0, 0, b.w, b.h, b.l });
        makeStatic();
        setBatch(tex);
    }

    void addColliders(vector<Collider*>& cs, float dt) override {
        cs.push_back(&coll);
    }

    virtual void draw(Sketch& s, float dt) override {
        draw::sprite(_sprite);
        draw::cube(s, bounds().x, bounds().y, bounds().z,
                   bounds().w, bounds().h, bounds().l);
        draw::sprite();
    }
};

class ObjectPillar : public ObjectBox {
    vec4 _side, _top;
public:
    ObjectPillar(box b, Texture* tex, vec4 side, vec4 top): 
        ObjectBox(b, tex, side), _side(side), _top(top) {
    }

    virtual void draw(Sketch& s, float dt) override {
        draw::topsprite(_top);
        draw::sidesprite(_side);
        draw::cube(s, bounds().x, bounds().y, bounds().z,
                   bounds().w, bounds().h, bounds().l);
        draw::sprite();
    }
};

class ObjectLamp : public Object, public Listener<light::PollEvent> {
    BoxCollider coll;
public:
    ObjectLamp(vec3 pos): Object(pos), coll({}) {
        makeStatic();
        setBounds({ -8, 0, -8, 16, 80, 16 });
        setBatch(TILE);
        coll = BoxCollider(bounds());
    }

    void handle(light::PollEvent& event) {
        event.addLight({
            { pos[0], pos[1] + 72, pos[2] },
            { 0.9375, 0.875, 1, 1 },
            128
        });
        event.addLight({
            { pos[0], pos[1] + 72, pos[2] },
            { 0.9375, 0.875, 1, 0.5 },
            384
        });
    }

    void addColliders(vector<Collider*>& cs, float dt) override {
        cs.push_back(&coll);
    }

    virtual void draw(Sketch& s, float dt) override {
        draw::setStretch(true);
        draw::sidesprite(128, 16, 12, 2);
        draw::topsprite(144, 16, 8, 8);
        draw::bottomsprite(144, 16, 8, 8);
        draw::cyl(s, pos[0] - 8, pos[1], pos[2] - 8, 16, 4, 16, 6);
        draw::sidesprite(112, 24, 8, 8);
        draw::topsprite(144, 16, 8, 8);
        draw::bottomsprite(144, 16, 8, 8);
        draw::cyl(s, pos[0] - 6, pos[1] + 4, pos[2] - 6, 12, 12, 12, 6);
        draw::sidesprite(116, 16, 2, 8);
        draw::cyl(s, pos[0] - 2, pos[1] + 16, pos[2] - 2, 4, 16, 4, 6);
        draw::sidesprite(118, 16, 2, 8);
        draw::cyl(s, pos[0] - 2, pos[1] + 32, pos[2] - 2, 4, 16, 4, 6);
        draw::sidesprite(118, 16, 2, 8);
        draw::cyl(s, pos[0] - 2, pos[1] + 48, pos[2] - 2, 4, 16, 4, 6);
        draw::sprite(120, 24, 4, 4);
        draw::ball(s, pos[0] - 4, pos[1] + 72, pos[2] - 4, 8, 8, 8, 7, 5);
        draw::sprite(128, 16, 12, 12);
        draw::ball(s, pos[0] - 12, pos[1] + 64, pos[2] - 12, 24, 24, 24, 9, 7);
        draw::setStretch(false);
    }

};

class ObjectFence : public Object {
    bool xoriented = false;
    BoxCollider coll;
public:
    ObjectFence(vec3 pos, bool orient): Object(pos), coll({}) {
        makeStatic();
        setBatch(TILE);
        xoriented = orient;
        if (xoriented) setBounds({ 0, 0, 8, 32, 32, 16 });
        else setBounds({ 8, 0, 0, 16, 32, 32 });
        coll = BoxCollider(bounds());
    }

    void addColliders(vector<Collider*>& cs, float dt) override {
        cs.push_back(&coll);
    }

    virtual void draw(Sketch& s, float dt) override {
        draw::sprite(0, 16, 16, 16);
        if (xoriented) {
            draw::northsprite(96, 64, 16, 16);
            draw::southsprite(96, 64, 16, 16);
            draw::cube(s, pos[0], pos[1], pos[2] + 15, 32, 128, 2);
        }
        else {
            draw::westsprite(96, 64, 16, 16);
            draw::eastsprite(96, 64, 16, 16);
            draw::cube(s, pos[0] + 15, pos[1], pos[2], 2, 128, 32);
        }
    }
};

class ObjectSign : public Object {
    int signs[4];
    bool xoriented = false;
    vec2 _root;
public:
    ObjectSign(vec2 root, vec3 pos, bool orient): Object(pos), _root(root) {
        for (int i = 0; i < 4; i ++) {
            signs[i] = rng::i(6);
        }
        makeStatic();
        setBatch(TILE);
        xoriented = orient;
    }

    virtual void draw(Sketch& s, float dt) override {
        draw::setStretch(true);
        draw::sprite(192, 16, 16, 16);
        draw::cube(s, pos[0] + 14, pos[1], pos[2] + 14, 4, 36, 4);
        float h = 36;
        for (int i = 0; i < 4; i ++) {
            if (signs[i] == 0) {
                draw::sprite(_root[0] + 192, _root[1] + 16, 16, 16);
                draw::cube(s, pos[0] + 14, pos[1] + h, pos[2] + 14, 4, 16, 4);
                h += 16;
            }
            else if (signs[i] == 1) {
                draw::sprite(_root[0] + 192, _root[1] + 16, 16, 16);
                draw::cube(s, pos[0] + 14, pos[1] + h, pos[2] + 14, 4, 4, 4);
                draw::setUnlit(true);
                if (xoriented) {
                    draw::northsprite(_root[0] + 160, _root[1], 16, 32);
                    draw::southsprite(_root[0] + 160, _root[1], 16, 32);
                    draw::cube(s, pos[0], pos[1] + h + 4, pos[2] + 13, 32, 64, 6);
                }
                else {
                    draw::westsprite(_root[0] + 160, _root[1], 16, 32);
                    draw::eastsprite(_root[0] + 160, _root[1], 16, 32);
                    draw::cube(s, pos[0] + 13, pos[1] + h + 4, pos[2], 6, 64, 32);
                }
                draw::setUnlit(false);
                h += 68;
            }
            else {
                draw::sprite(_root[0] + 192, _root[1] + 16, 16, 16);
                draw::cube(s, pos[0] + 14, pos[1] + h, pos[2] + 14, 4, 4, 4);
                draw::setUnlit(true);
                if (xoriented) {
                    draw::northsprite(_root[0] + 144 + signs[i] * 16, _root[1], 16, 16);
                    draw::southsprite(_root[0] + 144 + signs[i] * 16, _root[1], 16, 16);
                    draw::cube(s, pos[0], pos[1] + h + 4, pos[2] + 13, 32, 32, 6);
                }
                else {
                    draw::westsprite(_root[0] + 144 + signs[i] * 16, _root[1], 16, 16);
                    draw::eastsprite(_root[0] + 144 + signs[i] * 16, _root[1], 16, 16);
                    draw::cube(s, pos[0] + 13, pos[1] + h + 4, pos[2], 6, 32, 32);
                }
                draw::setUnlit(false);
                h += 36;
            }
        }
        draw::setStretch(false);
    }
};

class ObjectLitSign : public ObjectSign, public Listener<light::PollEvent> {
    bool fuchsia = false;
public:
    ObjectLitSign(vec2 root, vec3 pos, bool xoriented): ObjectSign(root, pos, xoriented),
        fuchsia(rng::i(2) == 0) {
        //
    }

    void handle(light::PollEvent& event) {
        if (fuchsia) {
            event.addLight({
                { pos[0], pos[1] + 36, pos[2] },
                { 1.0, 0.125, 0.875, 1 },
                192
            });
        }
        else {
            event.addLight({
                { pos[0], pos[1] + 36, pos[2] },
                { 0.125, 0.875, 1.0, 1 },
                192
            });
        }
    }
};

class GuiObjectText : public Object {
    const char* _text;
    float donetime = 0;
    bool space = true, done = false;
    bool escape = true;
    vec4 _sprite;
public:
    GuiObjectText(vec4 talksprite, const char* text): Object(vec3(0)), _text(text), _sprite(talksprite) {
        noclip();
        WORLD->pause();
    }

    ~GuiObjectText() {
        WORLD->unpause();
    }

    virtual void update(float dt) {
        Object::update(dt);
        if (donetime > 0 && donetime - dt <= 0) kill();
        else donetime -= dt;
        if (age() > 1.25 && !space && !done && input::keydown(GLFW_KEY_SPACE)) {
            done = true, donetime = 1.0;
        }
        if (!escape && input::keydown(GLFW_KEY_ESCAPE)) kill();
        space = input::keydown(GLFW_KEY_SPACE);
        escape = input::keydown(GLFW_KEY_ESCAPE);
    }

    virtual void draw(float dt) {
        const int WIDTH = 480, HEIGHT = 320;
        int h = HEIGHT;
        if (age() < 1) {
            h -= 128 * (1 - (1 - age()) * (1 - age()));
        }
        else if (done) {
            h -= 128;
            h += 128 * (1 - (donetime) * (donetime));
        }
        else if (alive()) {
            h -= 128;
        }
        glDepthMask(false);
        Sketch s;
        WHITE->bind();
        draw::color(0, 0, 0, 1);
        draw::rect(s, 0, h, WIDTH, 128);
        s.draw();
        WHITE->unbind();
        draw::color();
        GUI_TEXTURE->bind();
        s.clear();
        if (age() > 1.25) {
            draw::sprite(0, fmod(age(), 1) < 0.5 ? 0 : 16, 48, 16);
            draw::rect(s, WIDTH - 102, h + 128 - 38, 96, 32);
        }
        draw::sprite(_sprite);
        draw::rect(s, 0, h, 128, 128);
        s.draw();
        GUI_TEXTURE->unbind();
        FONT->bind();
        s.clear();
        draw::text(s, 134, h + 6, _text, 16, 24, 340);
        s.draw();
        FONT->unbind();
        glDepthMask(true);
    }
};

class ObjectGlow : public Object {
    float dim;
    float maxage;
    vec4 _color;
public:
    ObjectGlow(vec3 pos, vec4 color, float size, float speed, float age): 
        Object(pos), dim(size), maxage(age), _color(color) {
        vel = vec3{ rng::f(-speed, speed), rng::f(-speed, speed), rng::f(-speed, speed) };
        setBatch(GLOW);
    }

    virtual void update(float dt) override {
        Object::update(dt);
        if (age() > maxage) kill();
    }

    virtual void draw(Sketch& s, float dt) override {
        float coeff = (1 - age() / maxage);
        float size = dim * coeff;
        draw::setStretch(true);
        draw::sprite(0, 0, 64, 64);
        draw::color(_color[0], _color[1], _color[2], coeff);
        draw::board(s, pos[0], pos[1], pos[2], size, size);
        draw::color();
        draw::setStretch(false);
    }
};

struct Tiles {
    vector<void(*)(ObjectLayer*, int, int)> tiles;
    vector<void(*)(ObjectLayer*, int, int)> enemies;
    vector<void(*)(ObjectLayer*, int, int)> objects;
public:
    Tiles(): tiles(256, nullptr), enemies(256, nullptr), objects(256, nullptr) {}

    void tile(int value, void(*func)(ObjectLayer*, int, int)) {
        tiles[value] = func;
    }
    
    void enemy(int value, void(*func)(ObjectLayer*, int, int)) {
        enemies[value] = func;
    }
    
    void object(int value, void(*func)(ObjectLayer*, int, int)) {
        objects[value] = func;
    }

    void generate(ObjectLayer* l, int x, int z, int r, int g, int b) {
        if (enemies[r]) enemies[r](ENEMIES, x, z);
        if (tiles[g]) tiles[g](TERRAIN, x, z);
        if (objects[b]) objects[b](TERRAIN, x, z);
    }
};

Tiles LEVEL_1, LEVEL_2, LEVEL_3, LEVEL_4;
Tiles* LEVEL = &LEVEL_1;

class ObjectEnemy : public Object {
    int _maxhealth, _health;
public:
    ObjectEnemy(vec3 pos, int maxhealth): Object(pos), _maxhealth(maxhealth), _health(_maxhealth) {}

    virtual void knockback(vec3 amt) {
        vel = amt;
    }

    virtual void die() {
        kill();
    }

    virtual void hurt(int damage) {
        _health -= damage;
        SOUND_ENEMY_HURT->play(0.6);
        if (_health <= 0) die();
    }

    virtual void heal(int healing) {
        _health += healing;
        if (_health > _maxhealth) _health = _maxhealth;
    }

    int health() const {
        return _health;
    }

    int maxhealth() const {
        return _maxhealth;
    }
};  

class ObjectBullet : public Object, public Listener<light::PollEvent> {
    float angle = 0;

    void splash() {
        // TODO: fun particle effect
    }
public:
    ObjectBullet(vec3 pos, float yaw): Object(pos) {
        vel[0] = sin(radians(yaw)) * 1280;
        vel[1] = 0;
        vel[2] = cos(radians(yaw)) * 1280;  
        angle = yaw;
    }

    virtual void handle(light::PollEvent& event) {
        event.addLight({
            pos + vec3{8, 8, 8},
            { 1, 0.875, 0.25, 0.25 },
            128
        });
        event.addLight({
            pos + vec3{8, 8, 8},
            { 1, 0.875, 0.25, 1.0 },
            48
        });
        // event.addLight({
        //     pos + vec3{8, 8, 8},
        //     { 0.25, 0.5, 1, 0.125 },
        //     128
        // });
        // event.addLight({
        //     pos + vec3{8, 8, 8},
        //     { 0.25, 0.5, 1, 0.5 },
        //     48
        // });
    }

    virtual void update(float dt) override {
        Object::update(dt);
        unordered_set<Object*> enemies = ENEMIES->objectsNear(bounds().expand(4));
        if (enemies.size() > 0) {
            ((ObjectEnemy*)*enemies.begin())->hurt(1);
            ((ObjectEnemy*)*enemies.begin())->knockback({
                vel[0] / 8, 160, vel[2] / 8
            });
            splash(), kill();
        }
        if (age() > 1) kill();
        if (esc[0] != 0 || esc[1] != 0 || esc[2] != 0) splash(), kill(); 
    }

    virtual void draw(float dt) override {
        ENTITY->bind();
        draw::setStretch(true);

        transform::push();
        Sketch s;
        transform::shift(pos[0] + 8, pos[1] + 8, pos[2] + 8);
        transform::yrot(angle);
        transform::xrot(-90);
        draw::sprite({ 80, 16, 6, 8 });
        draw::topsprite({ 88, 16, 2, 2 });
        draw::bottomsprite({ 88, 16, 2, 2 });
        draw::cube(s, -4, -8, -4, 8, 16, 8);
        s.draw();
        transform::pop();

        draw::setStretch(false);
        ENTITY->unbind();
    }
};

class ObjectPlayer : public Object, public Listener<light::PollEvent> {
    BoxCollider coll;
    bool lmouse = false, rmouse = false;
    bool grounded = true;
    float camera = 45;
    float angle = 0;
    int _health = 6, _maxhealth = 6;
    vec2 relvel;
    float dmgtimer = 0;
    friend class ObjectRobot;
public:
    ObjectPlayer(): Object({ 0, 32, 0 }), coll(bounds()) {
        setBounds({ 0, 0, 0, 24, 24, 24 });
        FOOTSTEPS->play();
        FOOTSTEPS->setVolume(0);
    }

    int health() const {
        return _health;
    }

    int maxhealth() const {
        return _maxhealth;
    }

    void die();

    void hurt(int damage) {
        if (dmgtimer <= 0) {
            _health -= damage;
            dmgtimer = 1;
            if (_health <= 0) die();
        }
    }

    void heal(int amount) {
        _health += amount;
        if (_health > _maxhealth) _health = _maxhealth;
    }

    void spawnAt(vec3 p) {
        pos = p;
    }

    virtual void handle(light::PollEvent& event) {
        event.addLight({
            pos + vec3{8, 32, 8},
            { 1, 0.9375, 0.625, 0.25 },
            256
        });
    }

    virtual void update(float dt) override;

    virtual void draw(float dt) override {
        ENTITY->bind();
        if (dmgtimer > 0 && fmod(dmgtimer * 8, 1) > 0.5) WHITE->bind();

        float speed = vec2(vel[0], vel[2]).dist() / 180;
        vec2 tiltspeed;
        tiltspeed[0] = relvel[0] * sin(radians(camera)) + relvel[1] * cos(radians(camera));
        tiltspeed[2] = relvel[0] * cos(radians(camera)) + relvel[1] * -sin(radians(camera));

        FOOTSTEPS->setVolume(0.2 * speed);
        FOOTSTEPS->setPitch(1.2);
        if (dt == 0) FOOTSTEPS->setVolume(0);

        draw::setStretch(true);
        transform::push();
        transform::shift(pos[0] + 12, pos[1], pos[2] + 12);
        transform::scale(2.0);
        transform::zrot((-relvel[1] / 180) * 5);
        transform::xrot((relvel[0] / 180) * 5);
        transform::yrot(-camera + 90);
        transform::yrot(angle);

        Sketch s;

        s.clear();
        // head
        draw::southsprite({ 16, 8, 8, 8 });
        draw::northsprite({ 0, 8, 8, 8 });
        draw::westsprite({ 8, 8, 8, 8 });
        draw::eastsprite({ 24, 8, 8, 8 });
        draw::topsprite({ 16, 0, 8, 8 });
        draw::bottomsprite({ 16, 16, 8, 8 });
        draw::cube(s, -4, 16, -4, 8, 8, 8);

        // collar
        draw::sidesprite({ 9, 24, 9, 2 });
        draw::topsprite({ 0, 24, 9, 9 });
        draw::bottomsprite({ 0, 35, 9, 9 });
        draw::southsprite({ 0, 33, 9, 2 });
        draw::cube(s, -4.5, 15, -4.5, 9, 2, 9);

        // chest
        draw::southsprite({ 43, 0, 7, 10 });
        draw::northsprite({ 32, 0, 7, 10 });
        draw::westsprite({ 39, 0, 4, 10 });
        draw::eastsprite({ 50, 0, 4, 10 });
        draw::topsprite({ 36, 12, 7, 4 });
        draw::bottomsprite({ 43, 12, 7, 4 });
        draw::segm(s, -3.5, 8, -2, 7, 8, 4, 1.3, 1);
        draw::sidesprite({ 54, 0, 7, 3 });
        draw::topsprite({ 54, 3, 3, 3 });
        draw::bottomsprite({ 61, 0, 3, 3 });
        draw::segm(s, -3.5, 6, -2, 7, 2, 4, 1, 1.3);

        // belt
        // draw::southsprite({ 64, 21, 8, 2 });
        // draw::northsprite({ 64, 21, 8, 2 });
        // draw::westsprite({ 64, 23, 5, 2 });
        // draw::eastsprite({ 64, 23, 5, 2 });
        // draw::topsprite({ 64, 16, 8, 5 });
        // draw::bottomsprite({ 72, 16, 8, 5 });
        // draw::cube(s, -4, 18.5, -2.5, 8, 2, 5);
        s.draw();

        // larm
        transform::push();
        s.clear();
        transform::shift(-3, 14, 0);
        transform::yrot(40);
        transform::zrot(15);
        transform::xrot(-80);
        transform::zrot(-10);
        draw::southsprite({ 67, 0, 3, 10 });
        draw::northsprite({ 73, 0, 3, 10 });
        draw::westsprite({ 64, 0, 3, 10 });
        draw::eastsprite({ 70, 0, 3, 10 });
        draw::topsprite({ 64, 10, 3, 3 });
        draw::bottomsprite({ 67, 10, 3, 3 });
        draw::cube(s, -2, -8, -2, 3, 10, 3);
        s.draw();
        transform::pop();

        // rarm
        transform::push();
        s.clear();
        transform::shift(3, 14, 0);
        transform::yrot(-25);
        transform::xrot(-80);
        transform::zrot(10);
        draw::southsprite({ 67, 0, 3, 10 });
        draw::northsprite({ 73, 0, 3, 10 });
        draw::westsprite({ 64, 0, 3, 10 });
        draw::eastsprite({ 70, 0, 3, 10 });
        draw::topsprite({ 64, 10, 3, 3 });
        draw::bottomsprite({ 67, 10, 3, 3 });
        draw::cube(s, -1, -8, -2, 3, 10, 3);
        s.draw();
        transform::push();
        transform::xrot(-10);
        transform::zrot(10);
        s.clear();
        draw::southsprite({ 92, 4, 4, 5 });
        draw::northsprite({ 84, 4, 4, 5 });
        draw::westsprite({ 80, 4, 4, 5 });
        draw::eastsprite({ 88, 4, 4, 5 });
        draw::topsprite({ 94, 0, 4, 4 });
        draw::bottomsprite({ 85, 0, 4, 4 });
        draw::cube(s, -2, -11, -1, 4, 5, 4);
        s.draw();
        transform::pop();
        transform::pop();

        float legangle = 30 * sin(age() * 10) * speed;

        // lleg
        transform::push();
        s.clear();
        transform::shift(-2, 8, 0);
        transform::xrot(legangle);
        draw::southsprite({ 68, 32, 4, 8 });
        draw::northsprite({ 76, 32, 4, 8 });
        draw::westsprite({ 64, 32, 4, 8 });
        draw::eastsprite({ 72, 32, 4, 8 });
        draw::topsprite({ 64, 28, 4, 4 });
        draw::bottomsprite({ 68, 28, 4, 4 });
        draw::cube(s, -2, -8, -2, 4, 8, 4);
        s.draw();
        transform::pop();

        // rleg
        transform::push();
        s.clear();
        transform::shift(2, 8, 0);
        transform::xrot(-legangle);
        draw::southsprite({ 68, 32, 4, 8 });
        draw::northsprite({ 76, 32, 4, 8 });
        draw::westsprite({ 64, 32, 4, 8 });
        draw::eastsprite({ 72, 32, 4, 8 });
        draw::topsprite({ 64, 28, 4, 4 });
        draw::bottomsprite({ 68, 28, 4, 4 });
        draw::cube(s, -2, -8, -2, 4, 8, 4);
        s.draw();
        transform::pop();

        draw::color();
        draw::setStretch(false);

        transform::pop();
        
        ENTITY->unbind();
    }
};

class GuiObjectHealth : public Object {
public:
    GuiObjectHealth(): Object(vec3(0)) {
        noclip();
    }

    virtual void update(float dt) {
        Object::update(dt);
    }

    virtual void draw(float dt) {
        FONT->bind();
        Sketch s;
        char buffer[50];
        sprintf(buffer, "HP: %d/%d", PLAYER->health(), PLAYER->maxhealth());
        draw::color(1, 1, 1, 1);
        draw::text(s, 6, 6, buffer, 16, 24, 468);
        s.draw();
        FONT->unbind();
        glDepthMask(true);
    }
};

class ObjectRobot : public Object, public Listener<light::PollEvent> {
    int _level;
    float angle;
    bool held = true;
    float shoottimer = 0;
    friend class ObjectPlayer;
public:
    ObjectRobot(vec3 pos, int level): Object(pos), _level(level) {
        setBounds({ 0, 0, 0, 16, 16, 16 });
    }

    void spawnAt(vec3 p) {
        pos = p;
    }

    void hold() {
        held = true;
    }

    void detach() {
        held = false;
    }

    void levelup() {
        ++ _level;
    }

    virtual void handle(light::PollEvent& event) override {
        if (_level > 0) {
            event.addLight({
                pos + vec3{8, 8, 8},
                { 1, 0.875, 0.25, 0.25 },
                192
            });
            event.addLight({
                pos + vec3{8, 8, 8},
                { 1, 0.875, 0.25, 0.75 },
                48
            });
        }
    }

    virtual void update(float dt) {
        Object::update(dt);
        if (held) {
            angle = PLAYER->angle - PLAYER->camera - 90;
            pos = PLAYER->pos + vec3(4 + 8 * sin(radians(angle)), 16, 4 + 8 * cos(radians(angle)));
            vel = vec3(0);

            if (_level == 1 || _level == 2) {
                auto entities = ENEMIES->dynamicIn(bounds().expand(256));
                vector<Object*> entityvec(entities.begin(), entities.end());
                sort(entityvec.begin(), entityvec.end(), [&](Object* a, Object* b) -> bool {
                    return (a->bounds().center() - bounds().center()).distsq() < (b->bounds().center() - bounds().center()).dist();
                });
                if (entityvec.size()) {
                    vec3 diff = (bounds().center() - entityvec[0]->bounds().center());
                    diff[1] = 0;
                    vec3 look = vec3(sin(radians(angle)), 0, cos(radians(angle)));
                    if (diff.dist() < 320 && dot(look, diff) < 0 && shoottimer <= 0) {
                        vec3 dv = diff.normalize() * -480;
                        float bulletdir = degrees(atan2(dv[0], dv[2]));
                        ENTITIES->add(new ObjectBullet(pos + vec3(0, 12, 0)
                            + vec3(8 * sin(radians(bulletdir)), 0, 8 * cos(radians(bulletdir))), 
                            bulletdir));
                        shoottimer = 0.5;
                    }
                }
            }
        }
        else {
            vel[1] -= 640.0f * dt;
            if (_level == 1 || _level == 2) {
                auto entities = ENEMIES->dynamicIn(bounds().expand(256));
                vector<Object*> entityvec(entities.begin(), entities.end());
                sort(entityvec.begin(), entityvec.end(), [&](Object* a, Object* b) -> bool {
                    return (a->bounds().center() - bounds().center()).distsq() < (b->bounds().center() - bounds().center()).dist();
                });
                if (entityvec.size()) {
                    vec3 diff = (bounds().center() - entityvec[0]->bounds().center());
                    diff[1] = 0;

                    if (diff.dist() < 320 && shoottimer <= 0) {
                        vec3 dv = diff.normalize() * -480;
                        angle = degrees(atan2(dv[0], dv[2]));
                        
                        float bulletdir = angle + rng::f(-15, 15);
                        ENTITIES->add(new ObjectBullet(pos + vec3(0, 12, 0)
                            + vec3(8 * sin(radians(bulletdir)), 0, 8 * cos(radians(bulletdir))), 
                            bulletdir));
                        shoottimer = 0.5;
                    }
                }
            }
            if (_level == 2) {
                vec3 diff = bounds().center() - PLAYER->bounds().center();
                diff[1] = 0;
                if (diff.dist() > 64) {
                    vec3 dv = diff.normalize() * -480;
                    vel += dv * dt;
                    angle = degrees(atan2(dv[0], dv[2]));
                    if (vel[0] < -180) vel[0] = -180;
                    if (vel[0] > 180) vel[0] = 180;
                    if (vel[2] < -180) vel[2] = -180;
                    if (vel[2] > 180) vel[2] = 180;
                }
            }
            else {
                vel[0] -= vel[0] > 0 ? 640 * dt : -640 * dt; 
                vel[2] -= vel[2] > 0 ? 640 * dt : -640 * dt; 
                if (vel[0] >= -640 * dt && vel[0] <= 640 * dt) vel[0] = 0;
                if (vel[2] >= -640 * dt && vel[2] <= 640 * dt) vel[2] = 0;
            }
        };
        if (shoottimer > 0) shoottimer -= dt;
    }

    virtual void draw(float dt) {
        ENTITY->bind();

        int height = 3;
        if (_level == 2) height += 5;

        draw::setStretch(true);
        transform::push();
        transform::shift(pos[0] + 8, pos[1] + height * 2, pos[2] + 8);
        transform::scale(2.0);
        transform::yrot(angle);

        Sketch s;
        if (_level == 0) draw::southsprite(12, 48, 6, 6);
        else draw::southsprite(6, 54, 6, 6);
        draw::westsprite(0, 54, 6, 6);
        draw::eastsprite(12, 54, 6, 6);
        draw::northsprite(18, 54, 6, 6);
        draw::topsprite(6, 48, 6, 6);
        draw::bottomsprite(6, 60, 6, 6);
        draw::cube(s, -3, -3, -3, 6, 6, 6);
        s.draw();

        transform::push();
        transform::xrot(30);
        s.clear();
        draw::sidesprite({ 27, 48, 1, 6 });
        draw::topsprite({ 27, 48, 1, 1 });
        draw::bottomsprite({ 27, 48, 1, 1 });
        draw::cube(s, -0.5, 2.5, -0.5, 1, 6, 1);
        if (_level == 0) {
            draw::sidesprite(28, 48, 2, 2);
            draw::topsprite(30, 48, 2, 2);
            draw::bottomsprite(30, 50, 2, 2);
        }
        else {
            draw::sidesprite(28, 52, 2, 2);
            draw::topsprite(30, 52, 2, 2);
            draw::bottomsprite(30, 54, 2, 2);
        }
        draw::cube(s, -1, 8, -1, 2, 2, 2);
        s.draw();
        if (_level == 2) {
            Sketch s;
            draw::sprite(16, 64, 2, 2);
            draw::cube(s, -1, -4, -1, 2, 2, 2);
            draw::southsprite(10, 76, 10, 4);
            draw::northsprite(10, 76, 10, 4);
            draw::topsprite(10, 66, 10, 10);
            draw::bottomsprite(10, 66, 10, 10);
            draw::westsprite(0, 76, 10, 4);
            draw::eastsprite(0, 76, 10, 4);
            draw::cube(s, -5, -8, -5, 10, 4, 10);
            s.draw();
        }
        transform::pop();
        transform::pop();

        draw::setStretch(false);

        ENTITY->unbind();
    }
};

void changeLevel(Tiles* level);

class GuiObjectLevelChange : public Object {
    Tiles* dst;
public:
    GuiObjectLevelChange(Tiles* dest): Object(vec3(0)), dst(dest) {
        WORLD->pause();
        noclip();
    }

    virtual ~GuiObjectLevelChange() {
    }

    virtual void update(float dt) override {
        Object::update(dt);
        if (age() < 3 && age() + dt >= 3) changeLevel(dst), CURRENT_MUSIC->setVolume(0), WORLD->unpause();
        if (age() < 1) {
            while (CURRENT_MUSIC && CURRENT_MUSIC->getVolume() > 0) {
                CURRENT_MUSIC->setVolume(max(0.0f, CURRENT_MUSIC->getVolume() - dt));
            }
        }
        else if (age() >= 4) {
            while (CURRENT_MUSIC && CURRENT_MUSIC->getVolume() < 1) {
                CURRENT_MUSIC->setVolume(min(1.0f, CURRENT_MUSIC->getVolume() + dt));
            }
        }
        if (age() >= 5) kill();
    }
    
    virtual void draw(float dt) override {
        WHITE->bind();
        if (age() < 1) {
            Sketch s;
            draw::color(0, 0, 0, age());
            draw::rect(s, 0, 0, 480, 320);
            draw::color();
            s.draw();
        }
        else if (age() >= 4) {
            Sketch s;
            draw::color(0, 0, 0, 1 - (age() - 4));
            draw::rect(s, 0, 0, 480, 320);
            draw::color();
            s.draw();
        }
        else {
            Sketch s;
            draw::color(0, 0, 0, 1);
            draw::rect(s, 0, 0, 480, 320);
            draw::color();
            s.draw();
        }
        WHITE->unbind();
    }
};

class GuiObjectGameOver : public Object {
public:
    GuiObjectGameOver(): Object(vec3(0)) {
        WORLD->pause();
        SOUND_PLAYER_DEATH->play();
        noclip();
    }

    virtual void update(float dt) override {
        Object::update(dt);
        if (age() < 5 && age() + dt >= 5) {
            PLAYER->spawnAt({ 100, 100, 100 });
            changeLevel(&LEVEL_1), CURRENT_MUSIC->setVolume(0), WORLD->unpause();
            if (ROBOT) CHARACTER->remove(ROBOT);
            ROBOT = nullptr;
            PLAYER->heal(99999);
        }
        if (age() < 1) {
            while (CURRENT_MUSIC && CURRENT_MUSIC->getVolume() > 0) {
                CURRENT_MUSIC->setVolume(max(0.0f, CURRENT_MUSIC->getVolume() - dt));
            }
        }
        else if (age() >= 6) {
            while (CURRENT_MUSIC && CURRENT_MUSIC->getVolume() < 1) {
                CURRENT_MUSIC->setVolume(min(1.0f, CURRENT_MUSIC->getVolume() + dt));
            }
        }
        if (age() >= 7) kill();
    }
    
    virtual void draw(float dt) override {
        WHITE->bind();
        float a = 0;
        if (age() < 1) a = age();
        else if (age() >= 6) a = (1 - (age() - 6));
        else a = 1;
        Sketch s;
        draw::color(0, 0, 0, a);
        draw::rect(s, 0, 0, 480, 320);
        draw::color();
        s.draw();
        WHITE->unbind();
        FONT->bind();
        s.clear();
        draw::color(1, 1, 1, a);
        draw::text(s, 136, 148, "Game over! :(", 16, 24, 800);
        draw::color();
        s.draw();
        FONT->unbind();
    }
};

void ObjectPlayer::update(float dt) {
    view::setCamera(pos + vec3{ 12, 20, 12 }, camera, -30);
    Object::update(dt);
    coll = BoxCollider(bounds());

    angle = degrees(atan2(input::mvec()[1], input::mvec()[0]));

    if (input::mousedown(GLFW_MOUSE_BUTTON_LEFT)) {
        if (!lmouse) {
            SOUND_PLAYER_SHOOT->play(0.8);
            float bulletdir = angle - camera + 90;
            ENTITIES->add(new ObjectBullet(pos + vec3(4, 16, 4)
                + vec3(8 * sin(radians(bulletdir)), 0, 8 * cos(radians(bulletdir))), 
                angle - camera + 90));
        }
        lmouse = true;
    }
    else lmouse = false;

    if (input::mousedown(GLFW_MOUSE_BUTTON_RIGHT)) {
        if (!rmouse) {
            if (ROBOT && ROBOT->_level > 0) {
                if (ROBOT->held) ROBOT->detach(), SOUND_BOTDOWN->play();
                else if (ROBOT->bounds().intersects(bounds().expand(64))) ROBOT->hold(), SOUND_BOTUP->play();
            }
        }
        rmouse = true;
    }
    else rmouse = false;

    if (input::keydown(GLFW_KEY_Q)) camera -= 180 * dt;
    if (input::keydown(GLFW_KEY_E)) camera += 180 * dt;

    if (input::keydown(GLFW_KEY_D)) relvel[0] += 1280 * dt;
    else if (input::keydown(GLFW_KEY_A)) relvel[0] -= 1280 * dt;
    else {
        relvel[0] -= relvel[0] > 0 ? 640 * dt : -640 * dt; 
        if (relvel[0] >= -1280 * dt && relvel[0] <= 1280 * dt) relvel[0] = 0;
    }
    if (relvel[0] > 180) relvel[0] = 180;
    if (relvel[0] < -180) relvel[0] = -180;
    
    if (input::keydown(GLFW_KEY_S)) relvel[1] += 1280 * dt;
    else if (input::keydown(GLFW_KEY_W)) relvel[1] -= 1280 * dt;
    else {
        relvel[1] -= relvel[1] > 0 ? 640 * dt : -640 * dt; 
        if (relvel[1] >= -1280 * dt && relvel[1] <= 1280 * dt) relvel[1] = 0;
    }
    if (relvel[1] > 180) relvel[1] = 180;
    if (relvel[1] < -180) relvel[1] = -180;
    
    vel[0] = relvel[1] * -sin(radians(-camera)) + relvel[0] * cos(radians(-camera));
    vel[2] = relvel[1] * -cos(radians(-camera)) + relvel[0] * -sin(radians(-camera));

    if (esc[1] > 0.00001) grounded = true;
    if (input::keydown(GLFW_KEY_SPACE) && grounded) grounded = false, vel[1] = 200.0f;

    vel[1] -= 640.0f * dt;

    if (dmgtimer > 0) dmgtimer -= dt;
}

void ObjectPlayer::die() {
    GUI->add(new GuiObjectGameOver());
}

class ObjectBouncer : public ObjectEnemy, public Listener<light::PollEvent> {
    bool xorient = false;
    float sign = 1;
    float stationary = 0;
public:
    ObjectBouncer(vec3 pos, bool xoriented): ObjectEnemy(pos, 1000), xorient(xoriented) {
        if (xoriented) setBounds({ 0, 0, 0, 48, 32, 32 }), vel[0] = 240;
        else setBounds({ 0, 0, 0, 32, 32, 48 }), vel[2] = 240;
        pos[1] += 1;
    }

    virtual void knockback(vec3 amt) {
        //
    }

    virtual void hurt(int damage) {
        SOUND_INVULN->play(0.6);
    }

    virtual void handle(light::PollEvent& event) {
        float off = vel.distsq() < 0.001 ? 0.3 : 1;
        event.addLight({
            { pos[0] + 16, pos[1] + 16, pos[2] + 16 },
            { 1.0, 0.5, 0.25, 0.75 * off },
            48
        });
        event.addLight({
            { pos[0] + 16, pos[1] + 16, pos[2] + 16 },
            { 1.0, 0.5, 0.25, 0.25 * off },
            128
        });
    }

    virtual void update(float dt) override {
        Object::update(dt);
        if (esc.distsq() > 0.001) {
            stationary = 1.0;
        }
        if (stationary > 0 && stationary - dt <= 0) {
            stationary = 0;
            sign *= -1;
            if (xorient) vel[0] = sign * 240;
            else vel[2] = sign * 240;
            pos += vel * dt;
        }
        else stationary -= dt;

        if (PLAYER->bounds().intersects(bounds())) PLAYER->hurt(1);
    }

    virtual void draw(float dt) override {
        ENTITY->bind();
        draw::setStretch(true);

        float off = vel.distsq() < 0.001 ? 16 : 0;

        transform::push();
        if (xorient) transform::shift(pos[0] + 24, pos[1] + 16, pos[2] + 16);
        else transform::shift(pos[0] + 16, pos[1] + 16, pos[2] + 24);
        transform::scale(2.0);
        if (xorient) transform::zrot(90);
        else transform::xrot(90);
        Sketch s;
        draw::sidesprite(80, 64 + off, 12, 16);
        draw::topsprite(96, 64 + off, 12, 12);
        draw::bottomsprite(96, 64 + off, 12, 12);
        draw::cube(s, -6, -8, -6, 12, 16, 12);
        draw::sidesprite(112, 64, 14, 2);
        draw::topsprite(112, 66, 14, 14);
        draw::bottomsprite(112, 66, 14, 14);
        draw::cube(s, -7, -9, -7, 14, 2, 14);
        draw::cube(s, -7, 7, -7, 14, 2, 14);
        draw::sprite(128, 64, 12, 16);
        draw::segm(s, -6, -16, -6, 12, 8, 12, 0, 1);
        draw::segm(s, -6, 8, -6, 12, 8, 12, 1, 0);
        s.draw();
        transform::pop();

        draw::setStretch(false);
        ENTITY->unbind();
    }
};

class ObjectDrone : public ObjectEnemy, public Listener<light::PollEvent> {
    float angle;
    float ontimer = 1;
public:
    ObjectDrone(vec3 pos): ObjectEnemy(pos, 4) {
        setBounds({ 0, 0, 0, 16, 32, 16 });
        deactivate();
    }

    virtual void handle(light::PollEvent& event) {
        event.addLight({
            { pos[0] + 8, pos[1] + 32, pos[2] + 8 },
            { 1.0, 0.5, 0.25, 0.75 },
            48
        });
        event.addLight({
            { pos[0] + 8, pos[1] + 32, pos[2] + 8 },
            { 1.0, 0.5, 0.25, 0.25 },
            128
        });
    }

    virtual void update(float dt) {
        Object::update(dt);

        vel[1] -= 640.0f * dt;
        vec3 diff = (bounds().center() - PLAYER->bounds().center());
        diff[1] = 0;
        float dist = diff.dist();
        if (dist > 400) {
            deactivate();
            return;
        }
        else {
            activate();
        }

        if (dist < 320) {
            ontimer -= dt;
            if (ontimer <= 0) {
                vec3 dv = diff.normalize() * -480;
                vel += dv * dt;
                angle = degrees(atan2(dv[0], dv[2]));
                if (vel[0] < -160) vel[0] = -160;
                if (vel[0] > 160) vel[0] = 160;
                if (vel[2] < -160) vel[2] = -160;
                if (vel[2] > 160) vel[2] = 160;
            }
        }
        else {
            vel[0] -= vel[0] > 0 ? 160 * dt : -160 * dt; 
            vel[2] -= vel[2] > 0 ? 160 * dt : -160 * dt; 
            if (vel[0] >= -160 * dt && vel[0] <= 160 * dt) vel[0] = 0;
            if (vel[2] >= -160 * dt && vel[2] <= 160 * dt) vel[2] = 0;
        }

        if (PLAYER->bounds().intersects(bounds())) PLAYER->hurt(1);
    }

    virtual void draw(float dt) {
        if ((bounds().center() - PLAYER->bounds().center()).dist() >= 400) return;
        ENTITY->bind();
        draw::setStretch(true);

        transform::push();
        transform::shift(pos[0] + 8, pos[1], pos[2] + 8);
        transform::scale(2.0);
        transform::zrot((-vel[0] / 160) * 10);
        transform::xrot((-vel[2] / 160) * 10);
        transform::yrot(angle);
        Sketch s;
        draw::sidesprite(48, 48, 7, 20);
        draw::southsprite(55, 48, 7, 20);
        draw::topsprite(71, 57, 7, 7);
        draw::bottomsprite(64, 57, 7, 7);
        draw::segm(s, -3.5, 8, -3.5, 7, 20, 7, 1.25, 1);
        if (ontimer <= 0 || fmod(ontimer * 4, 1) < 0.5) {
            draw::southsprite(51, 71, 4, 4);
        }
        else if (fmod(ontimer * 8, 1) >= 0.5) {
            draw::southsprite(58, 71, 4, 4);
        }
        draw::westsprite(48, 71, 3, 4);
        draw::eastsprite(55, 71, 3, 4);
        draw::topsprite(51, 68, 4, 3);
        draw::bottomsprite(51, 75, 4, 3);
        draw::cube(s, -2, 22, 2, 4, 4, 3);
        draw::sprite(64, 48, 2, 2);
        draw::cube(s, -0.5, 28, -0.5, 1, 2, 1);
        if (ontimer <= 0 || fmod(ontimer * 4, 1) < 0.5) {
            draw::sidesprite(64, 54, 4, 3);
            draw::topsprite(76, 48, 4, 4);
            draw::bottomsprite(72, 48, 4, 4);
            draw::cube(s, -2, 30, -2, 4, 3, 4);
            draw::sidesprite(64, 52, 4, 2);
            draw::southsprite(68, 52, 4, 2);
            draw::topsprite(68, 48, 4, 4);
            draw::bottomsprite(76, 48, 4, 4);
            draw::segm(s, -2, 33, -2, 4, 2, 4, 1, 0.5);
        }
        else if (fmod(ontimer * 4, 1) >= 0.5) {
            draw::sidesprite(80, 54, 4, 3);
            draw::topsprite(92, 48, 4, 4);
            draw::bottomsprite(88, 48, 4, 4);
            draw::cube(s, -2, 30, -2, 4, 3, 4);
            draw::sidesprite(80, 52, 4, 2);
            draw::southsprite(84, 52, 4, 2);
            draw::topsprite(84, 48, 4, 4);
            draw::bottomsprite(92, 48, 4, 4);
            draw::segm(s, -2, 33, -2, 4, 2, 4, 1, 0.5);
        }
        draw::sprite(64, 48, 2, 2);
        draw::cube(s, -0.5, 6, -0.5, 1, 2, 1);
        s.draw();

        transform::push();
        s.clear();
        transform::shift(0, 3.5, 0);
        transform::zrot(90);
        draw::topsprite(64, 64, 7, 7);
        draw::bottomsprite(64, 64, 7, 7);
        draw::sidesprite(64, 50, 2, 2);
        draw::cyl(s, -3.5, -1, -3.5, 7, 2, 7, 5);
        s.draw();
        transform::pop();

        transform::pop();

        draw::setStretch(false);
        ENTITY->unbind();
    }
};

class ObjectHeavyDrone : public ObjectEnemy, public Listener<light::PollEvent> {
    float angle;
    float ontimer = 1;
public:
    ObjectHeavyDrone(vec3 pos): ObjectEnemy(pos, 10) {
        setBounds({ 0, 0, 0, 24, 48, 24 });
        deactivate();
    }

    virtual void handle(light::PollEvent& event) {
        event.addLight({
            { pos[0] + 8, pos[1] + 32, pos[2] + 8 },
            { 1.0, 0.5, 0.25, 0.75 },
            48
        });
        event.addLight({
            { pos[0] + 8, pos[1] + 32, pos[2] + 8 },
            { 1.0, 0.5, 0.25, 0.25 },
            128
        });
    }

    virtual void update(float dt) {
        Object::update(dt);

        vel[1] -= 640.0f * dt;
        vec3 diff = (bounds().center() - PLAYER->bounds().center());
        diff[1] = 0;
        float dist = diff.dist();
        if (dist > 400) {
            deactivate();
            return;
        }
        else {
            activate();
        }

        if (dist < 320) {
            ontimer -= dt;
            if (ontimer <= 0) {
                vec3 dv = diff.normalize() * -480;
                vel += dv * dt;
                angle = degrees(atan2(dv[0], dv[2]));
                if (vel[0] < -80) vel[0] = -80;
                if (vel[0] > 80) vel[0] = 80;
                if (vel[2] < -80) vel[2] = -80;
                if (vel[2] > 80) vel[2] = 80;
            }
        }
        else {
            vel[0] -= vel[0] > 0 ? 80 * dt : -80 * dt; 
            vel[2] -= vel[2] > 0 ? 80 * dt : -80 * dt; 
            if (vel[0] >= -80 * dt && vel[0] <= 80 * dt) vel[0] = 0;
            if (vel[2] >= -80 * dt && vel[2] <= 80 * dt) vel[2] = 0;
        }

        if (PLAYER->bounds().intersects(bounds())) PLAYER->hurt(1);
    }

    virtual void draw(float dt) {
        if ((bounds().center() - PLAYER->bounds().center()).dist() >= 400) return;
        ENTITY->bind();
        draw::setStretch(true);

        transform::push();
        transform::shift(pos[0] + 8, pos[1], pos[2] + 8);
        transform::scale(2.0);
        transform::zrot((-vel[0] / 80) * 5);
        transform::xrot((-vel[2] / 80) * 5);
        transform::yrot(angle);
        Sketch s;
        draw::sidesprite(48, 96, 7, 20);
        draw::southsprite(55, 96, 7, 20);
        draw::topsprite(71, 105, 7, 7);
        draw::bottomsprite(64, 105, 7, 7);
        draw::segm(s, -3.5, 8, -3.5, 7, 20, 7, 1.25, 1);
        if (ontimer <= 0 || fmod(ontimer * 4, 1) < 0.5) {
            draw::southsprite(51, 119, 4, 4);
        }
        else if (fmod(ontimer * 8, 1) >= 0.5) {
            draw::southsprite(58, 119, 4, 4);
        }
        draw::westsprite(48, 119, 3, 4);
        draw::eastsprite(55, 119, 3, 4);
        draw::topsprite(51, 116, 4, 3);
        draw::bottomsprite(51, 123, 4, 3);
        draw::cube(s, -2, 22, 2, 4, 4, 3);
        draw::sprite(64, 96, 2, 2);
        draw::cube(s, -0.5, 28, -0.5, 1, 2, 1);
        if (ontimer <= 0 || fmod(ontimer * 4, 1) < 0.5) {
            draw::sidesprite(64, 102, 4, 3);
            draw::topsprite(76, 96, 4, 4);
            draw::bottomsprite(72, 96, 4, 4);
            draw::cube(s, -2, 30, -2, 4, 3, 4);
            draw::sidesprite(64, 100, 4, 2);
            draw::southsprite(68, 100, 4, 2);
            draw::topsprite(68, 96, 4, 4);
            draw::bottomsprite(76, 96, 4, 4);
            draw::segm(s, -2, 33, -2, 4, 2, 4, 1, 0.5);
        }
        else if (fmod(ontimer * 4, 1) >= 0.5) {
            draw::sidesprite(80, 102, 4, 3);
            draw::topsprite(92, 96, 4, 4);
            draw::bottomsprite(88, 96, 4, 4);
            draw::cube(s, -2, 30, -2, 4, 3, 4);
            draw::sidesprite(80, 100, 4, 2);
            draw::southsprite(84, 100, 4, 2);
            draw::topsprite(84, 96, 4, 4);
            draw::bottomsprite(92, 96, 4, 4);
            draw::segm(s, -2, 33, -2, 4, 2, 4, 1, 0.5);
        }
        draw::sprite(64, 96, 2, 2);
        draw::cube(s, -0.5, 6, -0.5, 1, 2, 1);
        s.draw();

        transform::push();
        s.clear();
        transform::shift(0, 3.5, 0);
        transform::zrot(90);
        draw::topsprite(64, 112, 7, 7);
        draw::bottomsprite(64, 112, 7, 7);
        draw::sidesprite(64, 98, 2, 2);
        draw::cyl(s, -3.5, -1, -3.5, 7, 2, 7, 5);
        s.draw();
        transform::pop();

        transform::pop();

        draw::setStretch(false);
        ENTITY->unbind();
    }
};

class ObjectSpawn : public Object {
public:
    ObjectSpawn(vec3 pos): Object(pos) {}

    virtual void update(float dt) {
        PLAYER->spawnAt(pos);
        if (ROBOT) ROBOT->spawnAt(pos);
        kill();
    }

    virtual void draw(float dt) {}
};

class ObjectLight : public Object {
public:
    ObjectLight(float intensity): Object(vec3(0)) {
        light::setIntensity(intensity);
    }

    virtual void update(float dt) {
        kill();
    }

    virtual void draw(float dt) {}
};

class ObjectCutscene : public Object {
    int id;
    int stage = 0;
    float panProgress = 0;
    Object* tracking;

    vec3 _panstart, _panend;

    void pan(vec3 end) {
        _panstart = view::_pos;
        _panend = end;
        panProgress = 0;
    }

    void panUpdate(float dt) {
        panProgress += dt;
        if (panProgress > 1) panProgress = 1;
        vec3 diff = _panend - _panstart;
        diff *= 1 - (1 - panProgress) * (1 - panProgress);
        view::_pos = _panstart + diff;
    }

    bool pandone() {
        return panProgress >= 1;
    }
public:
    static const int FIND_ROBOT = 1, COPS_ARRIVE = 2, BATTERY_GET = 3, TREADS_GET = 4,
                     ESCAPE_LAB = 5, FINALE = 6;
    ObjectCutscene(vec3 pos, int cutsceneid): Object(pos), id(cutsceneid) {
        setBounds({ -256, -256, -256, 512, 512, 512 });
        noclip();
    }

    void update(float dt) override {
        if (PLAYER->bounds().intersects(bounds())) {
            if (id == FIND_ROBOT) {
                if (stage == 0) {
                    CHARACTER->pause();
                    vec3 campos = view::_pos;
                    GUI->add(new GuiObjectText({ 0, 32, 64, 64 }, "Whoa...what's that in the road?"));
                    ++ stage;
                    pan(pos);
                }
                else if (stage == 1) {
                    panUpdate(dt);
                    if (pandone()) ++ stage;
                }
                else if (stage == 2) {
                    GUI->add(new GuiObjectText({ 0, 32, 64, 64 }, "Looks like some kind of broken-down robot. Maybe I should pick it up."));
                    ++ stage;
                    pan(PLAYER->bounds().center());
                }
                else if (stage == 3) {
                    panUpdate(dt);
                    if (pandone()) ++ stage;
                }
                else if (stage == 4) {
                    CHARACTER->unpause();
                    kill();
                }
            }
            else if (id == COPS_ARRIVE) {
                if (stage == 0) {
                    ENEMIES->pause();
                    CHARACTER->pause();
                    vec3 campos = view::_pos;
                    GUI->add(new GuiObjectText({ 0, 32, 64, 64 }, "I wonder if it's worth anything..."));
                    ++ stage;
                }
                else if (stage == 1) {
                    GUI->add(new GuiObjectText({ 128, 32, 64, 64 }, "*BZZZT* HAND OVER THE ROBOT."));
                    ++ stage;
                }
                else if (stage == 2) {
                    tracking = ENEMIES->add(new ObjectDrone({ pos[0], pos[1], pos[2] + 319 }));
                    pan(tracking->bounds().center());
                    ++ stage;
                }
                else if (stage == 3) {
                    panUpdate(dt);
                    if (pandone()) {
                        ++ stage;
                        pan(PLAYER->bounds().center());
                    }
                }
                else if (stage == 4) {
                    panUpdate(dt);
                    if (pandone()) {
                        GUI->add(new GuiObjectText({ 0, 32, 64, 64 }, "(A police robot!)"));
                        ++ stage;
                        pan(tracking->bounds().center());
                    }
                }
                else if (stage == 5) {
                    panUpdate(dt);
                    if (pandone()) {
                        GUI->add(new GuiObjectText({ 0, 96, 64, 64 }, "*BZZZT* THE ROBOT MUST BE DESTROYED. DO NOT RESIST."));
                        pan(PLAYER->bounds().center());
                        ++ stage;
                    }
                }
                else if (stage == 6) {
                    panUpdate(dt);
                    if (pandone()) {
                        GUI->add(new GuiObjectText({ 128, 96, 64, 64 }, "(Use the MOUSE to aim your weapon, and LEFT CLICK to shoot.)"));
                        ++ stage;
                    }
                }
                else if (stage == 7) {
                    ENEMIES->unpause();
                    CHARACTER->unpause();
                    if (!ENEMIES->dynamicIn(bounds().expand(192)).size()) ++stage;
                }
                else if (stage == 8) {
                    CHARACTER->pause();
                    GUI->add(new GuiObjectText({ 0, 32, 64, 64 }, "That was close. Why are the police getting involved?"));
                    ++ stage;
                }
                else if (stage == 9) {
                    GUI->add(new GuiObjectText({ 0, 32, 64, 64 }, "Whatever this robot is, it might be worth hanging onto."));
                    ++ stage;
                }
                else if (stage == 10) {
                    CURRENT_MUSIC = MUSIC_L1;
                    CURRENT_MUSIC->play();
                    CHARACTER->unpause();
                    kill();
                }
            }
            else if (id == BATTERY_GET) {
                if (stage == 0) {
                    CHARACTER->pause();
                    GUI->add(new GuiObjectText({ 128, 96, 64, 64 }, "The BATTERY has powered your robot back on!"));
                    stage ++;
                }
                else if (stage == 1) {
                    GUI->add(new GuiObjectText({ 128, 96, 64, 64 }, "It will now cover your back, shooting at enemies behind you."));
                    stage ++;
                }
                else if (stage == 2) {
                    GUI->add(new GuiObjectText({ 128, 96, 64, 64 }, "You can now use RIGHT CLICK to drop your robot down as a turret."));
                    stage ++;
                }
                else if (stage == 3) {
                    GUI->add(new GuiObjectText({ 128, 96, 64, 64 }, "Once placed, you can use RIGHT CLICK to pick it back up when nearby."));
                    stage ++;
                }
                else if (stage == 4) {
                    GUI->add(new GuiObjectText({ 0, 32, 64, 64 }, "So, you're awake. Remember anything?"));
                    stage ++;
                }
                else if (stage == 5) {
                    GUI->add(new GuiObjectText({ 64, 32, 64, 64 }, "*BEEP*"));
                    stage ++;
                }
                else if (stage == 6) {
                    GUI->add(new GuiObjectText({ 0, 32, 64, 64 }, "Not the talking type..."));
                    stage ++;
                }
                else if (stage == 7) {
                    GUI->add(new GuiObjectText({ 0, 32, 64, 64 }, "I guess I'll go look for more parts."));
                    stage ++;
                }
                else if (stage == 8) {
                    CHARACTER->unpause();
                    GUI->add(new GuiObjectLevelChange(&LEVEL_2));
                    kill();
                }
            }
            else if (id == TREADS_GET) {
                if (stage == 0) {
                    CHARACTER->pause();
                    GUI->add(new GuiObjectText({ 128, 96, 64, 64 }, "The TREADS have restored your robot's mobility!"));
                    stage ++;
                }
                else if (stage == 1) {
                    GUI->add(new GuiObjectText({ 128, 96, 64, 64 }, "When placed, it can now move on its own, following you."));
                    stage ++;
                }
                else if (stage == 2) {
                    GUI->add(new GuiObjectText({ 128, 96, 64, 64 }, "...what was this robot built for?"));
                    stage ++;
                }
                else if (stage == 3) {
                    GUI->add(new GuiObjectText({ 0, 32, 64, 64 }, "You're really coming together, huh."));
                    stage ++;
                }
                else if (stage == 4) {
                    GUI->add(new GuiObjectText({ 0, 32, 64, 64 }, "The police really want you for some reason, but don't worry."));
                    stage ++;
                }
                else if (stage == 5) {
                    GUI->add(new GuiObjectText({ 0, 32, 64, 64 }, "We'll get out of here."));
                    stage ++;
                }
                else if (stage == 6) {
                    GUI->add(new GuiObjectText({ 64, 32, 64, 64 }, "..."));
                    stage ++;
                }
                else if (stage == 6) {
                    GUI->add(new GuiObjectText({ 0, 32, 64, 64 }, "Let's keep moving."));
                    stage ++;
                }
                else if (stage == 7) {
                    CHARACTER->unpause();
                    GUI->add(new GuiObjectLevelChange(&LEVEL_3));
                    kill();
                }
            }
            else if (id == ESCAPE_LAB) {
                if (stage == 0) {
                    CHARACTER->pause();
                    GUI->add(new GuiObjectText({ 0, 32, 64, 64 }, "That was close. Too close."));
                    stage ++;
                }
                else if (stage == 1) {
                    GUI->add(new GuiObjectText({ 0, 32, 64, 64 }, "We're not far from the road out of here."));
                    stage ++;
                }
                else if (stage == 2) {
                    GUI->add(new GuiObjectText({ 0, 32, 64, 64 }, "We'll walk off into the desert, and leave this city for good!"));
                    stage ++;
                }
                else if (stage == 3) {
                    GUI->add(new GuiObjectText({ 0, 32, 64, 64 }, "Whatever you are, whoever is hunting you, I'll make sure you don't fall into the wrong hands."));
                    stage ++;
                }
                else if (stage == 4) {
                    CHARACTER->unpause();
                    GUI->add(new GuiObjectLevelChange(&LEVEL_4));
                    kill();
                }
            }
            else if (id == FINALE) {
                if (stage == 0) {
                    CHARACTER->pause();
                    GUI->add(new GuiObjectText({ 128, 96, 64, 64 }, "The end!"));
                    stage ++;
                }
                else if (stage == 1) {
                    GUI->add(new GuiObjectText({ 128, 96, 64, 64 }, "Press SPACE to start over."));
                    stage ++;
                }
                else if (stage == 2) {
                    CHARACTER->unpause();
                    GUI->add(new GuiObjectLevelChange(&LEVEL_1));
                    kill();
                }
            }
        }
    }
};

class GuiObjectItem : public Object {
    const char* _text;
    float donetime = 0;
    bool space = true, done = false;
    int id = 0;
public:
    GuiObjectItem(int itemid, const char* text): Object(vec3(0)), _text(text), id(itemid) {
        noclip();
        SOUND_ITEM_COLLECT->play(0.7);
        WORLD->pause();
    }

    ~GuiObjectItem() {
        WORLD->unpause();
    }

    virtual void update(float dt) {
        Object::update(dt);
        if (donetime > 0 && donetime - dt <= 0) kill();
        else donetime -= dt;
        if (age() > 1.25 && !space && !done && input::keydown(GLFW_KEY_SPACE)) {
            done = true, donetime = 1.0;
        }
        space = input::keydown(GLFW_KEY_SPACE);
    }

    virtual void draw(float dt);
};

class ObjectItem : public Object, public Listener<light::PollEvent> {
    int id;
    bool found = false;
public:
    const static int ROBOT = 1, BATTERY = 2, TREADS = 3, MICROCHIP = 4;

    ObjectItem(vec3 pos, int itemid): Object(pos), id(itemid) {
        setBounds({ -8, 0, -8, 16, 16, 16 });    
    }
    
    virtual void handle(light::PollEvent& event) {
        event.addLight({
            pos,
            { 1, 0.9375, 0.625, 0.25 },
            256
        });
        event.addLight({
            pos,
            { 1, 0.9375, 0.625, 0.75 },
            48 + 16 * sin(age() * PI)
        });
    }

    virtual void update(float dt) {
        Object::update(dt);
        if (found == true) {
            kill();
        }
        if (!found && PLAYER->bounds().intersects(bounds())) {
            if (id == ROBOT) {
                GUI->add(new GuiObjectItem(ObjectItem::id, "Found ROBOT."));
                WORLD->add(new ObjectCutscene(pos, ObjectCutscene::COPS_ARRIVE));
                found = true;
                (::ROBOT = (ObjectRobot*)CHARACTER->add(new ObjectRobot(pos, 0)))->update(dt);
            }
            else if (id == BATTERY) {
                GUI->add(new GuiObjectItem(ObjectItem::id, "Found FUSION CELL."));
                ::ROBOT->levelup();
                WORLD->add(new ObjectCutscene(pos, ObjectCutscene::BATTERY_GET));
                found = true;
            }
            else if (id == TREADS) {
                GUI->add(new GuiObjectItem(ObjectItem::id, "Found TREADS."));
                ::ROBOT->levelup();
                WORLD->add(new ObjectCutscene(pos, ObjectCutscene::TREADS_GET));
                found = true;
            }
            else if (id == MICROCHIP) {
                GUI->add(new GuiObjectItem(ObjectItem::id, "Found MICROCHIP."));
                ::ROBOT->levelup();
                WORLD->add(new ObjectCutscene(pos, ObjectCutscene::ESCAPE_LAB));
                found = true;
            }
        }
    }

    virtual void draw(float dt) {
        ENTITY->bind();

        int height = 3;

        draw::setStretch(true);
        transform::push();
        transform::shift(pos[0], pos[1] + 16 + 8 * sin(age() * PI / 2), pos[2]);
        transform::scale(2.0);
        transform::yrot(age() * 60.0f);

        if (id == ROBOT) {
            Sketch s;

            draw::southsprite(12, 48, 6, 6);
            draw::westsprite(0, 54, 6, 6);
            draw::eastsprite(12, 54, 6, 6);
            draw::northsprite(18, 54, 6, 6);
            draw::topsprite(6, 48, 6, 6);
            draw::bottomsprite(6, 60, 6, 6);
            draw::cube(s, -3, -3, -3, 6, 6, 6);
            s.draw();

            transform::push();
            transform::xrot(30);
            s.clear();
            draw::sidesprite({ 27, 48, 1, 6 });
            draw::topsprite({ 27, 48, 1, 1 });
            draw::bottomsprite({ 27, 48, 1, 1 });
            draw::cube(s, -0.5, 2.5, -0.5, 1, 6, 1);
            draw::sidesprite(28, 48, 2, 2);
            draw::topsprite(30, 48, 2, 2);
            draw::bottomsprite(30, 50, 2, 2);
            draw::cube(s, -1, 8, -1, 2, 2, 2);
            transform::pop();
            s.draw();
        }
        else if (id == BATTERY) {
            Sketch s;
            draw::sidesprite(32, 53, 5, 8);
            draw::topsprite(32, 48, 5, 5);
            draw::bottomsprite(37, 48, 5, 5);
            draw::cube(s, -2.5, -4.5, -2.5, 5, 8, 5);
            draw::sidesprite(42, 48, 2, 1);
            draw::topsprite(44, 48, 2, 2);
            draw::bottomsprite(44, 50, 2, 2);
            draw::cube(s, -1, 3.5, -1, 2, 1, 2);
            s.draw();
        }
        else if (id == TREADS) {
            Sketch s;
            draw::sprite(16, 64, 2, 2);
            draw::cube(s, -1, 2, -1, 2, 2, 2);
            draw::southsprite(10, 76, 10, 4);
            draw::northsprite(10, 76, 10, 4);
            draw::topsprite(10, 66, 10, 10);
            draw::bottomsprite(10, 66, 10, 10);
            draw::westsprite(0, 76, 10, 4);
            draw::eastsprite(0, 76, 10, 4);
            draw::cube(s, -5, -2, -5, 10, 4, 10);
            s.draw();
        }
        else if (id == MICROCHIP) {
            Sketch s;
            draw::sidesprite(17, 87, 5, 2);
            draw::topsprite(17, 82, 5, 5);
            draw::bottomsprite(17, 82, 5, 5);
            draw::cube(s, -2.5, -0.5, -2.5, 5, 2, 5);
            draw::sidesprite(17, 87, 5, 2);
            draw::topsprite(23, 81, 9, 9);
            draw::bottomsprite(23, 90, 9, 1);
            draw::cube(s, -4.5, -1.5, -4.5, 9, 1, 9);
            s.draw();
        }

        transform::pop();

        draw::setStretch(false);

        ENTITY->unbind();
    }
};

void GuiObjectItem::draw(float dt) {
    const int WIDTH = 480, HEIGHT = 320;
    int h = HEIGHT, t = -128;
    if (age() < 1) {
        h -= 128 * (1 - (1 - age()) * (1 - age()));
        t += 224 * (1 - (1 - age()) * (1 - age()));
    }
    else if (done) {
        h -= 128;
        h += 128 * (1 - (donetime) * (donetime));
        t += 224;
        t -= 224 * (1 - (donetime) * (donetime));
    }
    else if (alive()) {
        h -= 128;
        t += 224;
    }
    glDepthMask(false);
    Sketch s;
    WHITE->bind();
    draw::color(0, 0, 0, 1);
    draw::rect(s, 0, h, WIDTH, 128);
    s.draw();
    WHITE->unbind();
    draw::color();
    GUI_TEXTURE->bind();
    s.clear();
    if (age() > 1.25) {
        draw::sprite(0, fmod(age(), 1) < 0.5 ? 0 : 16, 48, 16);
        draw::rect(s, WIDTH - 102, h + 128 - 38, 96, 32);
    }
    s.draw();
    GUI_TEXTURE->unbind();
    FONT->bind();
    s.clear();
    draw::text(s, 134, h + 6, _text, 16, 24, 340);
    s.draw();
    FONT->unbind();
    glDepthMask(true);

    transform::push();
    transform::shift(WIDTH / 2, t, 0);
    transform::scale(6.0);
    transform::xrot(30);
    transform::yrot(180 + age() * 90);
    transform::zrot(180);
    ObjectItem(vec3(0, -16, 0), id).draw(dt);
    transform::pop();
}

void load(int lx, int ly) {
    int tx = lx * 16, ty = ly * 16;
    int basex = tx * 32 - 256, basez = ty * 32 - 256;
    for (int i = 0; i < 16; i ++) {
        for (int j = 0; j < 16; j ++) {
            vec4 v = MAP->get(tx + i, ty + j) * 255;
            LEVEL->generate(WORLD, basex + i * 32, basez + j * 32, (int)v[0], (int)v[1], (int)v[2]);
        }
    }
}

void changeLevel(Tiles* level) {
    LEVEL = level;
    TERRAIN->clear();
    ENEMIES->clear();
    if (LEVEL == &LEVEL_1) {
        light::setIntensity(0.5);
        CURRENT_MUSIC = MUSIC_L1;
        for (int i = 0; i < 9; i ++) {
            for (int j = 0; j < 9; j ++) {
                load(i, j);
            }
        }
    }
    else if (LEVEL == &LEVEL_2) {
        light::setIntensity(0.5);
        CURRENT_MUSIC = MUSIC_L2;
        CURRENT_MUSIC->play();
        for (int i = 9; i < 20; i ++) {
            for (int j = 0; j < 9; j ++) {
                load(i, j);
            }
        }
    }
    else if (LEVEL == &LEVEL_3) {
        light::setIntensity(0.9);
        CURRENT_MUSIC = MUSIC_L3;
        CURRENT_MUSIC->play();
        for (int i = 20; i < 30; i ++) {
            for (int j = 0; j < 10; j ++) {
                load(i, j);
            }
        }
    }
    else if (LEVEL == &LEVEL_4) {
        light::setIntensity(0.9);
        CURRENT_MUSIC = MUSIC_L4;
        CURRENT_MUSIC->play();
        for (int i = 0; i < 3; i ++) {
            for (int j = 9; j < 14; j ++) {
                load(i, j);
            }
        }
    }
}

void initTilesets() {
    /*
     * LEVEL 1
     */

    // invisible wall
    LEVEL_1.tile(32, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectBox({ x, -32, z, 32, 256, 32 }, TILE, { 0, 16, 16, 16 }));
    });

    // asphalt
    LEVEL_1.tile(96, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectBox({ x, -32, z, 32, 32, 32 }, TILE, { 0, 0, 16, 16 }));
    });

    // sidewalk
    LEVEL_1.tile(128, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectPillar({ x, -32, z, 32, 32, 32 }, TILE,
            { 16, 16, 16, 8 }, 
            { 16, 0, 16, 16 }));
    });

    // short bldg
    LEVEL_1.tile(192, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectPillar({ x, -32, z, 32, 288, 32 }, TILE,
            { 32, 32, 16, 16 }, 
            { 32, 16, 16, 16 }));
    });

    // medium bldg
    LEVEL_1.tile(208, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectPillar({ x, -32, z, 32, 400, 32 }, TILE,
            { 80, 32, 16, 16 }, 
            { 80, 16, 16, 16 }));
    });

    // tall bldg
    LEVEL_1.tile(224, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectPillar({ x, -32, z, 32, 640, 32 }, TILE,
            { 96, 32, 16, 16 }, 
            { 96, 16, 16, 16 }));
    });

    // drone
    LEVEL_1.enemy(192, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectDrone({ x, 0, z }));
    });

    // light
    LEVEL_1.object(96, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectLamp({ x + 16, 0, z + 16 }));
    });

    // hsign
    LEVEL_1.object(128, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectSign({ 0, 0 }, { x, 0, z }, true));
    });

    // vsign
    LEVEL_1.object(160, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectSign({ 0, 0 }, { x, 0, z }, false));
    });

    // lithsign
    LEVEL_1.object(192, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectLitSign({ 0, 0 }, { x, 0, z }, true));
    });

    // litvsign
    LEVEL_1.object(208, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectLitSign({ 0, 0 }, { x, 0, z }, false));
    });

    // cutscene1
    LEVEL_1.object(224, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectCutscene({ x + 16, 0, z + 16 }, ObjectCutscene::FIND_ROBOT));
    });

    // battery
    LEVEL_1.object(232, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectItem({ x + 16, 0, z + 16 }, ObjectItem::BATTERY));
    });

    // robot
    LEVEL_1.object(240, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectItem({ x + 16, 0, z + 16 }, ObjectItem::ROBOT));
    });

    // spawn
    LEVEL_1.object(255, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectSpawn({ x, 0, z }));
    });

    /*
     * LEVEL 2
     */

    // invisible wall
    LEVEL_2.tile(32, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectBox({ x, -32, z, 32, 256, 32 }, TILE, { 0, 16, 16, 16 }));
    });

    // rust cross
    LEVEL_2.tile(96, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectBox({ x, -32, z, 32, 32, 32 }, TILE,
            { 80, 48, 16, 16 }));
    });

    // rust bezel
    LEVEL_2.tile(112, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectBox({ x, -32, z, 32, 32, 32 }, TILE,
            { 80, 64, 16, 16 }));
    });

    // concrete
    LEVEL_2.tile(128, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectBox({ x, -32, z, 32, 32, 32 }, TILE,
            { 96, 48, 16, 16 }));
    });

    // rusty wall
    LEVEL_2.tile(192, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectPillar({ x, -32, z, 32, 288, 32 }, TILE,
            { 32, 32, 16, 16 }, 
            { 32, 16, 16, 16 }));
    });

    // rusty beam
    LEVEL_2.tile(224, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectBox({ x, -32, z, 32, 32, 32 }, TILE,
            { 80, 48, 16, 16 }));
        w->add(new ObjectPillar({ x, 200, z, 32, 24, 32 }, TILE,
            { 64, 48, 16, 12 }, { 64, 16, 16, 16 }));
    });

    // rusty door
    LEVEL_2.tile(208, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectBox({ x, -32, z, 32, 32, 32 }, TILE,
            { 96, 48, 16, 16 }));
        w->add(new ObjectPillar({ x, 72, z, 32, 24, 32 }, TILE,
            { 32, 48, 16, 12 }, { 32, 16, 16, 16 }));
        w->add(new ObjectPillar({ x, 96, z, 32, 160, 32 }, TILE,
            { 32, 32, 16, 16 }, { 32, 16, 16, 16 }));
    });

    // tall bldg
    LEVEL_2.tile(240, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectPillar({ x, -32, z, 32, 640, 32 }, TILE,
            { 64, 32, 16, 16 }, 
            { 64, 16, 16, 16 }));
    });

    // spawn
    LEVEL_2.object(255, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectSpawn({ x, 0, z }));
    });

    // xbouncer
    LEVEL_2.enemy(128, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectBouncer({ x, 0, z }, true));
    });

    // zbouncer
    LEVEL_2.enemy(160, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectBouncer({ x, 0, z }, false));
    });

    // drone
    LEVEL_2.enemy(192, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectDrone({ x, 0, z }));
    });

    // light
    LEVEL_2.object(96, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectLamp({ x + 16, 0, z + 16 }));
    });

    // hsign
    LEVEL_2.object(128, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectSign({ 0, 32 }, { x, 0, z }, true));
    });

    // vsign
    LEVEL_2.object(160, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectSign({ 0, 32 }, { x, 0, z }, false));
    });

    // lithsign
    LEVEL_2.object(192, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectLitSign({ 0, 32 }, { x, 0, z }, true));
    });

    // litvsign
    LEVEL_2.object(208, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectLitSign({ 0, 32 }, { x, 0, z }, false));
    });

    // hsign
    LEVEL_2.object(32, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectFence({ x, 0, z }, true));
    });

    // vsign
    LEVEL_2.object(64, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectFence({ x, 0, z }, false));
    });

    // treads
    LEVEL_2.object(232, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectItem({ x + 16, 0, z + 16 }, ObjectItem::TREADS));
    });

    /*
     * LEVEL 3
     */

    // invisible wall
    LEVEL_3.tile(32, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectBox({ x, -32, z, 32, 256, 32 }, TILE, { 0, 16, 16, 16 }));
    });

    // ground
    LEVEL_3.tile(128, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectBox({ x, -32, z, 32, 32, 32 }, TILE, { 96, 16, 16, 16 }));
    });

    // wall1
    LEVEL_3.tile(192, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectBox({ x, -32, z, 32, 224, 32 }, TILE, { 80, 16, 16, 16 }));
    });

    // wall2
    LEVEL_3.tile(224, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectPillar({ x, -32, z, 32, 224, 32 }, TILE, 
            { 48, 32, 16, 16 }, { 48, 16, 16, 16 }));
    });

    // hfence
    LEVEL_3.object(64, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectFence({ x, 0, z }, true));
    });

    // vfence
    LEVEL_3.object(96, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectFence({ x, 0, z }, false));
    });

    // xbouncer
    LEVEL_3.enemy(128, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectBouncer({ x, 0, z }, true));
    });

    // zbouncer
    LEVEL_3.enemy(160, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectBouncer({ x, 0, z }, false));
    });

    // drone
    LEVEL_3.enemy(192, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectDrone({ x, 0, z }));
    });

    // heavydrone
    LEVEL_3.enemy(224, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectHeavyDrone({ x, 0, z }));
    });

    // light
    LEVEL_3.object(128, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectLamp({ x + 16, 0, z + 16 }));
    });

    // spawn
    LEVEL_3.object(255, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectSpawn({ x, 0, z }));
    });

    // chip
    LEVEL_3.object(232, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectItem({ x + 16, 0, z + 16 }, ObjectItem::MICROCHIP));
    });

    // lvl 4 lol

    LEVEL_4.tile(128, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectBox({ x, -32, z, 32, 224, 32 }, TILE, { 80, 80, 16, 16 }));
    });

    LEVEL_4.tile(192, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectBox({ x, -32, z, 32, 160, 32 }, TILE, { 80, 64, 16, 16 }));
    });

    // spawn
    LEVEL_4.object(255, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectSpawn({ x, 0, z }));
    });

    // cutscene
    LEVEL_4.object(192, [](ObjectLayer* w, int x, int z) {
        w->add(new ObjectCutscene({ x, 0, z }, ObjectCutscene::FINALE));
    });
}

int main(int argc, char** argv) {
    art::init();

    Window win = Window(1440, 960);

    initTextures();
    initLayers();
    initPasses(win);
    initTilesets();
    initAudio();

    light::setDirection({ 0.6, -1, -0.2 });

    changeLevel(&LEVEL_1);

    CHARACTER->add(PLAYER = new ObjectPlayer());
    GUI->add(new GuiObjectHealth());
    win.addTickable(WORLD);
    win.addTickable(GUI);
    win.run();
    
    freeTextures();
    freeLayers();
    freeAudio();
    art::free();
    return 0;
}