.PHONY: jam

jam:
	g++ -Wall -static -static-libgcc -static-libstdc++ -g3 -L. -L/mingw64/lib -I. -Iinclude GLAD/glad.c jam.cpp -o jam \
	-lopenal.dll -lalut.dll -lSOIL -lopengl32 -lgdi32 -lkernel32 -luser32 -lcomdlg32 -lglfw3.dll -lglfw3
